var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST get_coord_messages */
router.post('/', function(req, res, next) {
    console.log('Received get_coord_messages request: ' + JSON.stringify(req.body));
    let result = {};
    db.any({
        text: 'WITH mess_users AS (\
                SELECT user_id\
                FROM message\
                WHERE ABS(latitude - $2) < 0.00001 AND ABS(longitude - $3) < 0.00001\
                GROUP BY user_id\
            ),\
            following_table AS (\
                SELECT following_user_id, COUNT(*) AS following_count\
                FROM follow\
                GROUP BY following_user_id\
            ),\
            followed_table AS (\
                SELECT followed_user_id, COUNT(*) AS followed_count\
                FROM follow\
                GROUP BY followed_user_id\
            ),\
            followed_by_user(followed_by_user_id) AS (\
                SELECT followed_user_id\
                FROM follow\
                WHERE following_user_id = $1\
            )\
            SELECT r.user_id, username, name, surname, description, CASE WHEN followed_by_user_id IS NULL THEN false ELSE true END AS followed, coalesce(following_count, 0) AS following_count, coalesce(followed_count, 0) AS followed_count, img_version, encode(img::bytea, \'base64\') AS img\
            FROM registered_user AS r JOIN mess_users AS m ON r.user_id = m.user_id LEFT JOIN followed_by_user AS f ON r.user_id = f.followed_by_user_id\
                LEFT JOIN following_table ON r.user_id = following_user_id LEFT JOIN followed_table ON r.user_id = followed_user_id',
        values: [req.body.user_id, req.body.latitude, req.body.longitude]
        })
        .then(data => {
            console.log('Get coord messages first success', JSON.stringify(data));
            result = { users: data };
            
            db.any({
                text: 'WITH liked_num AS (\
                    SELECT message_id, COUNT(*)\
                    FROM liked\
                    GROUP BY message_id\
                ),\
                liking(message_id) AS (\
                    SELECT message_id\
                    FROM liked\
                    WHERE user_id = $1\
                )\
                SELECT m.user_id, m.message_id, message, latitude, longitude, location, time, coalesce(l.count, 0) AS like_count, CASE WHEN liking.message_id IS NULL THEN false ELSE true END AS liked\
                FROM message AS m LEFT JOIN liked_num AS l ON m.message_id = l.message_id LEFT JOIN liking ON m.message_id = liking.message_id\
                WHERE ABS(latitude - $2) < 0.00001 AND ABS(longitude - $3) < 0.00001\
                ORDER BY time DESC',
                values: [req.body.user_id, req.body.latitude, req.body.longitude]
                })
                .then(data => {
                    console.log('Get coord messages second success', JSON.stringify(data));
                    result = { ...result, messages: data };
                    res.json(result);
                })
                .catch(error => {
                    console.log('ERROR:', error);
                    res.status(400).send(error);
                });
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;