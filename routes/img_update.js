var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST img_update */
router.post('/', function(req, res, next) {
    console.log('Received img_update request: ' + JSON.stringify(req.body));
    db.one({
        text: 'SELECT * FROM img_update($1, $2)',
        values: [req.body.user_id, req.body.img]
        })
        .then(data => {
            console.log('Updated image');
            res.sendStatus(200);
        })
        .catch(error => {
            //TODO Check and differentiate errors
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;