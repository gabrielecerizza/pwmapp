var express = require('express');
var router = express.Router();

var createMessageRouter = require('./create_message');
var imgUpdateRouter = require('./img_update');
var followRouter = require('./follow');
var unfollowRouter = require('./unfollow');
var profileRouter = require('./profile');
var getFollowedMessagesRouter = require('./get_followed_messages');
var followedRouter = require('./followed');
var getImageRouter = require('./get_image');
var userIdCheckRouter = require('./user_id_check');
var getCoordMessagesRouter = require('./get_coord_messages');
var getUserDataRouter = require('./get_user_data');
var profileUpdateRouter = require('./profile_update');
var likeRouter = require('./like');
var unlikeRouter = require('./unlike');
var getLikedMessagesRouter = require('./get_liked_messages');
var searchRouter = require('./search');

/* API dispatcher */
router.use(userIdCheckRouter);
router.use('/create_message', createMessageRouter);
router.use('/img_update', imgUpdateRouter);
router.use('/follow', followRouter);
router.use('/unfollow', unfollowRouter);
router.use('/profile', profileRouter);
router.use('/get_followed_messages', getFollowedMessagesRouter);
router.use('/followed', followedRouter);
router.use('/get_image', getImageRouter);
router.use('/get_coord_messages', getCoordMessagesRouter);
router.use('/get_user_data', getUserDataRouter);
router.use('/profile_update', profileUpdateRouter);
router.use('/like', likeRouter);
router.use('/unlike', unlikeRouter);
router.use('/get_liked_messages', getLikedMessagesRouter);
router.use('/search', searchRouter);

module.exports = router;