var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST search */
router.post('/', function(req, res, next) {
    console.log('Received search request: ' + JSON.stringify(req.body));
    const re = /\s+/;
    let arr = req.body.search_text.split(re);
    let search_string = '(' + arr.join('|') + ')';

    db.any({
        text: 'WITH following_table AS (\
            SELECT following_user_id, COUNT(*) AS following_count\
            FROM follow\
            GROUP BY following_user_id\
        ),\
        followed_table AS (\
            SELECT followed_user_id, COUNT(*) AS followed_count\
            FROM follow\
            GROUP BY followed_user_id\
        ),\
        followed_by_user(followed_by_user_id) AS (\
            SELECT followed_user_id\
            FROM follow\
            WHERE following_user_id = $1\
        )\
        SELECT DISTINCT r.user_id, username, name, surname, description, CASE WHEN followed_by_user_id IS NULL THEN false ELSE true END AS followed, coalesce(following_count, 0) AS following_count, coalesce(followed_count, 0) AS followed_count, img_version, encode(img::bytea, \'base64\') AS img\
        FROM registered_user AS r LEFT JOIN following_table ON r.user_id = following_user_id LEFT JOIN followed_table ON r.user_id = followed_user_id\
            LEFT JOIN followed_by_user ON r.user_id = followed_by_user_id\
        WHERE name ~* $2 OR surname ~* $2 OR username ~* $2',
        values: [req.body.user_id, search_string]
        })
        .then(data => {
            console.log(data);
            res.json(data);
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;