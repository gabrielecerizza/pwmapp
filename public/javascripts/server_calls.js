import { MessagesAdapter, UsersAdapter } from './adapter';
import { stash } from './stash';
import { showPage, setLoggedInInterface, updateProfilePage, getSortAndFilterObject, loadMap,
    loadMapMessagesAndShowMap, showAlert } from './utility';
const TAG = 'server_calls:';
const BASE_URL = 'https://localhost' // :3000(http) - :443(https)

function sendGetUserDataRequest() {
    console.log(TAG, 'Called sendGetUserDataRequest');

    $.ajax({
        method: 'post',
        url: BASE_URL + '/api/get_user_data',
        data: {
            user_id: stash.userId,
            queried_user_id: stash.userId
        },
        dataType: 'json',
        success: function(result) {
            console.log(TAG, 'sendGetUserDataRequest success');
            stash.username = result.username;
            stash.name = result.name;
            stash.surname = result.surname;
            stash.img = result.img;

            setLoggedInInterface(loadMap);
        },
        error: function(result) {
            console.log(TAG, 'sendGetUserDataRequest error', JSON.stringify(result));
            showAlert('Connection error: could not retrieve user data');
        }
    });
}

function loadMapMessagesFromServer(callbackAfterMessages, callbackAfterShownMap) {
    console.log(TAG, 'Called loadMessagesFromServer');
    $.ajax({
        method: 'get',
        url: BASE_URL + '/get_map_messages',
        dataType: 'json',
        success: function(result) {
            console.log(TAG, 'loadMessagesFromServer success', JSON.stringify(result));

            stash.markers.forEach( marker => {
                marker.remove();
            });
            stash.markers = [];

            result.messages.forEach(element => {
                let markerDiv = document.createElement('div');
                markerDiv.className = 'map-marker-unfocus d-flex align-items-center justify-content-center';
                let markerInnerDiv = document.createElement('div');
                let txt = document.createTextNode(element.count);
                markerInnerDiv.className = 'map-marker-inner-div d-flex justify-content-center align-items-center';
                markerInnerDiv.append(txt);
                markerDiv.append(markerInnerDiv);

                markerDiv.addEventListener('click', function() {
                    console.log("clicked", element.latitude, element.longitude);
                    openMapCoordMessagesModal(element.latitude, element.longitude);
                });

                let marker = new mapboxgl.Marker(markerDiv).setLngLat([element.longitude, element.latitude]);
                const markerElement = marker.getElement();
                markerElement.addEventListener('mouseenter', () => {
                    markerDiv.className = 'map-marker-focus d-flex align-items-center justify-content-center';
                });
                markerElement.addEventListener('mouseleave', () => {
                    markerDiv.className = 'map-marker-unfocus d-flex align-items-center justify-content-center my-font';
                });
                marker.addTo(stash.map);
                stash.markers.push(marker);
            });

            if (callbackAfterMessages) callbackAfterMessages();
            if (callbackAfterShownMap) callbackAfterShownMap();
        },
        error: function(result) {
            console.log(TAG, 'loadMessagesFromServer error');
            showAlert('Connection error: could not retrieve messages');
        }
    });
}

function openMapCoordMessagesModal(lat, lon) {
    console.log(TAG, 'Called openMapCoordMessagesModal');
    $('#map-coord-messages-modal').modal();
    $('.modal-backdrop').addClass('modal-rel-backdrop').appendTo($('#map-coord-messages-modal').parent());

    let url, isUnregisteredUser;
    if (stash.userId) {
        url = BASE_URL + '/api/get_coord_messages';
        isUnregisteredUser = false;
    } else {
        url = BASE_URL + '/get_unregistered_coord_messages';
        isUnregisteredUser = true;
    }

    $.ajax({
        method: 'post',
        url: url,
        data: {
            user_id: stash.userId,
            latitude: lat,
            longitude: lon
        },
        dataType: 'json',
        success: function(result) {
            console.log(TAG, 'openMapCoordMessagesModal success', JSON.stringify(result.messages));

            let ad = new MessagesAdapter('#map-coord-messages-container', result.users, result.messages, isUnregisteredUser);
            stash.adapter = ad;
            stash.adapter.refresh(getSortAndFilterObject());

            //setMessagesContainerTitle(lat, lon);
            $('#map-coord-messages-modal-title').text(result.messages[0].location);
        },
        error: function(result) {
            console.log(TAG, 'openMapCoordMessagesModal error');
            showAlert('Connection error: could not retrieve messages');
        }
    });
}

function setMessagesContainerTitle(lat, lon) {
    console.log(TAG, 'Called setMessagesContainerTitle');

    $.ajax({
        method: 'get',
        url: 'https://nominatim.openstreetmap.org/reverse?lat='+lat+'&lon='+lon+'&format=json',
        success: function(result) {
            console.log(TAG, 'setMessagesContainerTitle success', JSON.stringify(result));
            if (result.error) {
                 // Clear modal title. This happens when location is in the sea etc.
                $('#map-coord-messages-modal-title').text('');
            } else {
                $('#map-coord-messages-modal-title').text(result.display_name);
            }
        },
        error: function(result) {
            console.log(TAG, 'setMessagesContainerTitle error');
            // Clear modal title. This happens when location is in the sea etc.
            $('#map-coord-messages-modal-title').text('');
        }
    });
}

function sendFollowRequest(element) {
    console.log(TAG, 'Called sendFollowRequest');

    $.ajax({
        method: 'post',
        url: BASE_URL + '/api/follow',
        data: {
            user_id: stash.userId,
            other_user_id: element.user_id
        },
        dataType: 'text',
        success: function(result) {
            console.log(TAG, 'sendFollowRequest success', result);

            function searchOtherUser (value, index, array) {
                return value.user_id == element.user_id;
            }
            function searchMyUser (value, index, array) {
                return value.user_id == stash.userId;
            }
            let otherUser = stash.adapter.users.find(searchOtherUser);
            if (otherUser) {
                otherUser.followed_count = Number(otherUser.followed_count) + 1;
                otherUser.followed = true;
            }
            
            let myUser = stash.adapter.users.find(searchMyUser);
            if (myUser) {
                let count = Number(myUser.following_count) + 1;
                myUser.following_count = count;
                $('#profile-page-following-number').text(count);
            }
            let followingCount = Number($('#profile-page-following-number').text()) + 1;
            $('#profile-page-following-number').text(followingCount);

            if (stash.adapter.refreshPopovers) {
                // For MessagesAdapter
                stash.adapter.refreshPopovers();
                // Needed to instantly refresh the popover currently on display
                $('#a' + element.message_id).popover('show');
            } else {
                // For UsersAdapter
                stash.adapter.refresh();
            }
        },
        error: function(result) {
            console.log(TAG, 'sendFollowRequest error', JSON.stringify(result));
            showAlert('Connection error: follow request failed');
        }
    });
}

function sendUnfollowRequest(element) {
    console.log(TAG, 'Called sendUnfollowRequest');

    $.ajax({
        method: 'post',
        url: BASE_URL + '/api/unfollow',
        data: {
            user_id: stash.userId,
            other_user_id: element.user_id
        },
        dataType: 'text',
        success: function(result) {
            console.log(TAG, 'sendUnfollowRequest success', result);

            function searchOtherUser (value, index, array) {
                return value.user_id == element.user_id;
            }
            function searchMyUser (value, index, array) {
                return value.user_id == stash.userId;
            }
            let otherUser = stash.adapter.users.find(searchOtherUser);
            if (otherUser) {
                otherUser.followed_count = Number(otherUser.followed_count) - 1;
                otherUser.followed = false;
            }
            
            let myUser = stash.adapter.users.find(searchMyUser);
            if (myUser) {
                let count = Number(myUser.following_count) - 1;
                myUser.following_count = count;
                $('#profile-page-following-number').text(count);
            }
            let followingCount = Number($('#profile-page-following-number').text()) - 1;
            $('#profile-page-following-number').text(followingCount);


            if (stash.adapter.refreshPopovers) {
                stash.adapter.refreshPopovers();
                stash.adapter.pruneUnfollowed(element);
            } else {
                stash.adapter.refresh();
            }
        },
        error: function(result) {
            console.log(TAG, 'sendUnfollowRequest error', JSON.stringify(result));
            showAlert('Connection error: unfollow request failed');
        }
    });
}

function sendCreateMessageRequest(lat, lon) {
    console.log(TAG, 'Called sendCreateMessageRequest', 'lat: ' + lat, 'lon: ' + lon);

    if ($('#message-textarea').val() == '') {
        showAlert('Error: empty messages are not allowed');
        stash.geocoder.clear();
        stash.lastGeocoderLocation = undefined;
        stash.sendReverseGeocodedMessage = false;
    } else {
        $.ajax({
            method: 'post',
            url: BASE_URL + '/api/create_message',
            data: {
                user_id: stash.userId,
                latitude: lat,
                longitude: lon,
                message: $('#message-textarea').val(),
                location: stash.lastGeocoderLocation || 'undefined'
            },
            dataType: 'text',
            success: function(result) {
                console.log(TAG, 'sendCreateMessageRequest success', result);
                $('#map-write-input-section').hide();
                $('#map-write-button').show();
                $('#map-close-write-button').hide();
                $('#message-textarea').val('');
                $('#latitude-input').val('');
                $('#longitude-input').val('');
                stash.geocoder.clear();
                stash.lastGeocoderLocation = undefined;
                stash.sendReverseGeocodedMessage = false;
    
                loadMapMessagesFromServer();
            },
            error: function(result) {
                console.log(TAG, 'sendCreateMessageRequest error', JSON.stringify(result));
                showAlert('Connection error: could not create message');
                stash.geocoder.clear();
                stash.lastGeocoderLocation = undefined;
                stash.sendReverseGeocodedMessage = false;
            }
        });
    }
}

function sendLoginRequest() {
    console.log(TAG, 'Called sendLoginRequest');

    $.ajax({
        method: 'post',
        url: BASE_URL + '/login',
        data: {
            username: $('#login-username-input').val(),
            password: $('#login-password-input').val()
        },
        dataType: 'json',
        success: function(result) {
            console.log(TAG, 'sendLoginRequest success', JSON.stringify(result));
            stash.username = $('#login-username-input').val();
            stash.userId = result.user_id;
            stash.name = result.name;
            stash.surname = result.surname;
            stash.img = result.img;
            localStorage.setItem('userId', stash.userId);

            $('.login-input').val('');

            setLoggedInInterface();
            loadMapMessagesAndShowMap();
        },
        error: function(result) {
            console.log(TAG, 'sendLoginRequest error', JSON.stringify(result));
            $('#login-error-div').show();
        }
    });
}

function sendRegisterRequest() {
    console.log(TAG, 'Called sendRegisterRequest');
    console.log(TAG, 'Description is:', $('#signup-description-input').val());

    $.ajax({
        method: 'post',
        url: BASE_URL + '/register',
        data: {
            username: $('#signup-username-input').val(),
            password: $('#signup-password-input').val(),
            name: $('#signup-name-input').val(),
            surname: $('#signup-surname-input').val(),
            description: $('#signup-description-input').val()
        },
        dataType: 'json',
        success: function(result) {
            console.log(TAG, 'sendRegisterRequest success', JSON.stringify(result));
            
            $('.input-input').val('');
            $('#signup-error-div').hide();
            $('#signup-successful-div').show();
            $('.input-count').text(0);

        },
        error: function(result) {
            console.log(TAG, 'sendRegisterRequest error', JSON.stringify(result));
            $('#signup-error-div').show();
        }
    });
}

function sendProfileRequest() {
    console.log(TAG, 'Called sendProfileRequest');

    $.ajax({
        method: 'post',
        url: BASE_URL + '/api/profile',
        data: {
            user_id: stash.userId,
            queried_user_id: stash.userId
        },
        dataType: 'json',
        success: function(result) {
            console.log(TAG, 'sendProfileRequest success', JSON.stringify(result));
            stash.name = result.user_data.name;
            stash.surname = result.user_data.surname;
            stash.img = result.user_data.img;
            stash.description = result.user_data.description;

            $('#profile-page-name').text(result.user_data.name + ' ' + result.user_data.surname);
            $('#profile-page-handle').text('@' + result.user_data.username);
            $('#profile-page-description').text(result.user_data.description);
            $('#profile-page-following-number').text(result.user_data.following_count);
            $('#profile-page-followed-number').text(result.user_data.followed_count);
            
            if (result.user_data.img) {
                //console.log('result img', result.user_data.img);
                $('#profile-page-image').attr('src', 'data:image/jpeg;base64,' + result.user_data.img);
            } else {
                console.log('result img', result.user_data.img);
                $('#profile-page-image').attr('src', 'default_user_icon.png');
            }
            
            result.messages.forEach(function(element) {
                element.name = result.user_data.name;
                element.surname = result.user_data.surname;
                element.username = result.user_data.username;
            });

            let ma = new MessagesAdapter('#profile-messages-section', [result.user_data], result.messages, false);
            stash.adapter = ma;
            
            ma.refresh(); // No need to pass filter here
            showPage('.profile-page');
        },
        error: function(result) {
            console.log(TAG, 'sendProfileRequest error', JSON.stringify(result));
            showAlert('Connection error: could not retrieve profile data');
            $('.nav-button').css('color', 'rgb(20, 23, 26)');
            $('#navbar-map-button').css('color', 'rgb(29, 161, 242)');
        }
    });
}

function sendProfileUpdateRequest(newName, newSurname, newDescription, newImg) {
    console.log(TAG, 'Called sendProfileUpdateRequest');

    $.ajax({
        method: 'post',
        url: BASE_URL + '/api/profile_update',
        data: {
            user_id: stash.userId,
            name: newName,
            surname: newSurname,
            img: newImg,
            description: newDescription
        },
        dataType: 'text',
        success: function(result) {
            console.log(TAG, 'sendProfileUpdateRequest success', result);
            
            $('#edit-profile-error-div').hide();
            $('#edit-profile-successful-div').show();

            stash.name = newName;
            stash.surname = newSurname;
            stash.img = newImg;
            stash.description = newDescription;

            updateProfilePage();
        },
        error: function(result) {
            console.log(TAG, 'sendProfileUpdateRequest error', JSON.stringify(result));
            
            $('#edit-profile-error-div').show();
        }
    });
}

function sendGetUserMessagesRequest() {
    console.log(TAG, 'Called sendGetUserMessagesRequest');

    $.ajax({
        method: 'post',
        url: BASE_URL + '/api/profile',
        data: {
            user_id: stash.userId,
            queried_user_id: stash.userId
        },
        dataType: 'json',
        success: function(result) {
            console.log(TAG, 'sendGetUserMessagesRequest success', JSON.stringify(result));
            
            result.messages.forEach(function(element) {
                element.name = result.user_data.name;
                element.surname = result.user_data.surname;
                element.username = result.user_data.username;
            });

            let ma = new MessagesAdapter('#profile-messages-section', [result.user_data], result.messages, false);
            stash.adapter = ma;

            ma.refresh();
        },
        error: function(result) {
            console.log(TAG, 'sendGetUserMessagesRequest error', JSON.stringify(result));
            showAlert('Connection error: could not retrieve user messages');
        }
    });
}

function sendGetFollowedMessagesRequest() {
    console.log(TAG, 'Called sendGetFollowedMessagesRequest');

    $.ajax({
        method: 'post',
        url: BASE_URL + '/api/get_followed_messages',
        data: {
            user_id: stash.userId
        },
        dataType: 'json',
        success: function(result) {
            console.log(TAG, 'sendGetFollowedMessagesRequest success', JSON.stringify(result));
            
            let ma = new MessagesAdapter('#profile-messages-section', result.followed_users, result.messages, false, false, true);
            stash.adapter = ma;

            ma.refresh();
        },
        error: function(result) {
            console.log(TAG, 'sendGetFollowedMessagesRequest error', JSON.stringify(result));
            showAlert('Connection error: could not retrieve followed messages');
        }
    });
}

function sendLikeRequest(message) {
    console.log(TAG, 'Called sendLikeRequest');

    $.ajax({
        method: 'post',
        url: BASE_URL + '/api/like',
        data: {
            user_id: stash.userId,
            message_id: message.message_id
        },
        dataType: 'text',
        success: function(result) {
            console.log(TAG, 'sendLikeRequest success', result);
            
            let messageToUpdate = stash.adapter.messages.find(function(value, index, array) {
                return value.message_id === message.message_id;
            })

            if (messageToUpdate) {
                messageToUpdate.like_count = Number(messageToUpdate.like_count) + 1;
                messageToUpdate.liked = true;
                stash.adapter.refresh(getSortAndFilterObject());
            } else {
                console.log(TAG, 'error updating message', 'shouldn\'t happen');
            }
        },
        error: function(result) {
            console.log(TAG, 'sendLikeRequest error', JSON.stringify(result));
            showAlert('Connection error: like request failed');
        }
    });
}

function sendUnlikeRequest(message) {
    console.log(TAG, 'Called sendUnlikeRequest');

    $.ajax({
        method: 'post',
        url: BASE_URL + '/api/unlike',
        data: {
            user_id: stash.userId,
            message_id: message.message_id
        },
        dataType: 'text',
        success: function(result) {
            console.log(TAG, 'sendUnlikeRequest success', result);

            let messageToUpdate = stash.adapter.messages.find(function(value, index, array) {
                return value.message_id === message.message_id;
            })

            if (messageToUpdate) {
                messageToUpdate.like_count = Number(messageToUpdate.like_count) - 1;
                messageToUpdate.liked = false;
                stash.adapter.refresh(getSortAndFilterObject());
            } else {
                console.log(TAG, 'error updating message', 'shouldn\'t happen');
            }
        },
        error: function(result) {
            console.log(TAG, 'sendUnlikeRequest error', JSON.stringify(result));
            showAlert('Connection error: unlike request failed');
        }
    });
}

function sendGetLikedMessagesRequest() {
    console.log(TAG, 'Called sendGetLikedMessagesRequest');

    $.ajax({
        method: 'post',
        url: BASE_URL + '/api/get_liked_messages',
        data: {
            user_id: stash.userId
        },
        dataType: 'json',
        success: function(result) {
            console.log(TAG, 'sendGetLikedMessagesRequest success', JSON.stringify(result));
            
            let ma = new MessagesAdapter('#profile-messages-section', result.liked_messages_users, result.messages, false, true);
            stash.adapter = ma;

            ma.refresh();
        },
        error: function(result) {
            console.log(TAG, 'sendGetLikedMessagesRequest error', JSON.stringify(result));
            showAlert('Connection error: could not retrieve liked messages');
        }
    });
}

function sendSearchRequest() {
    console.log(TAG, 'Called sendSearchRequest');

    $.ajax({
        method: 'post',
        url: BASE_URL + '/api/search',
        data: {
            user_id: stash.userId,
            search_text: $('#search-input').val()
        },
        dataType: 'json',
        success: function(result) {
            console.log(TAG, 'sendSearchRequest success', JSON.stringify(result));

            let ua = new UsersAdapter('#search-result-section', result);
            stash.adapter = ua;
            ua.refresh();
        },
        error: function(result) {
            console.log(TAG, 'sendSearchRequest error', JSON.stringify(result));
            showAlert('Connection error: search failed');
        }
    });
}

export { loadMapMessagesFromServer, loadMap, sendFollowRequest, sendUnfollowRequest,
    sendCreateMessageRequest, sendLoginRequest, sendRegisterRequest, sendProfileRequest, 
    sendGetUserDataRequest, sendProfileUpdateRequest, sendGetUserMessagesRequest, 
    sendGetFollowedMessagesRequest, sendLikeRequest, sendUnlikeRequest, sendGetLikedMessagesRequest,
    sendSearchRequest };