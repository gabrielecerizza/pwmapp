var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST get_followed_messages */
router.post('/', function(req, res, next) {
    console.log('Received get_followed_messages request: ' + JSON.stringify(req.body));
    let result = {};
    db.any({
        text: 'WITH following_table AS (\
                SELECT following_user_id, COUNT(*) AS following_count\
                FROM follow\
                GROUP BY following_user_id\
            ),\
            followed_table AS (\
                SELECT followed_user_id, COUNT(*) AS followed_count\
                FROM follow\
                GROUP BY followed_user_id\
            )\
            SELECT user_id, username, name, surname, description, true AS followed, coalesce(following_count, 0) AS following_count, coalesce(followed_count, 0) AS followed_count, img_version, encode(img::bytea, \'base64\') AS img\
            FROM registered_user AS r JOIN follow ON r.user_id = follow.followed_user_id\
                LEFT JOIN following_table ON r.user_id = following_table.following_user_id LEFT JOIN followed_table ON r.user_id = followed_table.followed_user_id\
            WHERE follow.following_user_id = $1',
        values: [req.body.user_id] 
        })
        .then(data => {
            console.log('get_followed_messages first success', data);
            result = { followed_users: data };

            db.any({
                text: 'WITH liked_num AS (\
                        SELECT message_id, COUNT(*)\
                        FROM liked\
                        GROUP BY message_id\
                    ),\
                    liking(message_id) AS (\
                        SELECT message_id\
                        FROM liked\
                        WHERE user_id = $1\
                    )\
                    SELECT m.user_id, m.message_id, latitude, longitude, location, message, time, coalesce(l.count, 0) AS like_count, CASE WHEN liking.message_id IS NULL THEN false ELSE true END AS liked\
                    FROM message AS m LEFT JOIN liked_num AS l ON m.message_id = l.message_id\
                        LEFT JOIN liking ON m.message_id = liking.message_id\
                    WHERE m.user_id IN (\
                        SELECT followed_user_id\
                        FROM follow\
                        WHERE following_user_id = $1\
                    ) ORDER BY time DESC;',
                values: [req.body.user_id]
                })
                .then(data => {
                    console.log('get_followed_messages second success', data);
                    result = { ...result, messages: data};
                    res.json(result);
                })
                .catch(error => {
                    console.log('ERROR:', error);
                    res.status(400).send(error);
                });
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;