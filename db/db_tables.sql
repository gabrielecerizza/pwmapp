CREATE TABLE registered_user (
    user_id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    username VARCHAR NOT NULL UNIQUE,
    password VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    surname VARCHAR NOT NULL,
    img BYTEA,
    img_version INTEGER NOT NULL,
    description TEXT
);

CREATE TABLE message (
  message_id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  user_id INTEGER NOT NULL,
  latitude REAL NOT NULL,
  longitude REAL NOT NULL,
  message TEXT NOT NULL,
  time TIMESTAMP WITH TIME ZONE NOT NULL,
  location TEXT NOT NULL,
  CONSTRAINT message_fk_user_id
    FOREIGN KEY(user_id) REFERENCES registered_user(user_id)
    ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE follow (
  following_user_id INTEGER,
  followed_user_id INTEGER,
  CONSTRAINT follow_pk
		PRIMARY KEY(following_user_id, followed_user_id),
	CONSTRAINT follow_fk_following_user_id
		FOREIGN KEY(following_user_id) REFERENCES registered_user(user_id)
		ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT follow_fk_followed_user_id
		FOREIGN KEY(followed_user_id) REFERENCES registered_user(user_id)
		ON UPDATE CASCADE ON DELETE CASCADE 
);

CREATE TABLE liked (
  user_id INTEGER,
  message_id INTEGER,
  CONSTRAINT liked_pk
    PRIMARY KEY(user_id, message_id),
  CONSTRAINT liked_fk_user_id
    FOREIGN KEY(user_id) REFERENCES registered_user(user_id)
    ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT liked_fk_message_id
    FOREIGN KEY(message_id) REFERENCES message(message_id)
    ON UPDATE CASCADE ON DELETE CASCADE  
);