var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* USE user_id_check */
router.use(function(req, res, next) {
    console.log('Checking user_id: ' + JSON.stringify(req.body));
    db.one({
        text: 'SELECT * FROM registered_user WHERE user_id = $1',
        values: [req.body.user_id]
        })
        .then(data => {
            console.log('Checking user_id success');
            next();
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(401).send({ error: 'No such user_id' });
        });
});

module.exports = router;