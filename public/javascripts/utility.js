import { stash } from './stash';
import { loadMapMessagesFromServer, sendCreateMessageRequest } from './server_calls';
const TAG = 'utility:';

function showPage(cssClass) {
    $('.page, #login-error-div').hide();
    $('#map-close-write-button-svg-container').trigger('click');
    $('#message-track-coord-icon').trigger('click');
    $('.modal').modal('hide');
    $('#global-alert-container').css('max-width', '900px');
    $('#search-input').val('');
    $('#search-result-section').html('');
    $(cssClass).show();
}

function showMap() {
    $('.nav-button').css('color', 'rgb(20, 23, 26)');
    $('#navbar-map-button').css('color', 'rgb(29, 161, 242)');
    showPage('.map-page');
    $('#global-alert-container').css('max-width', '100%');
}

function loadMapMessagesAndShowMap(callbackAfterShownMap) {
    console.log(TAG, 'Called loadMapMessagesAndShowMap');
    loadMapMessagesFromServer(showMap, callbackAfterShownMap);
}

function loadMap() {
    console.log(TAG, 'Called loadMap');

    mapboxgl.accessToken = 'pk.eyJ1IjoiZ2FicmllbGVjZXJpenphIiwiYSI6ImNrMzZlZGJzNjAwc3ozZHA5ZTBpY3BuajIifQ.GgEzBHYXxd-Z_IEr9gJVIA';
    let map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
            center: [9.18951, 45.46427], // starting position [lng, lat]
            zoom: 12 // starting zoom
    });
    let geolocate = new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true
            },
            trackUserLocation: true,
            showAccuracyCircle: false
    });
    map.addControl(geolocate);
    
    map.on('load', function() {
	    geolocate.trigger();
    });

    let geocoder = new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        mapboxgl: mapboxgl,
        minLength: 4,
        trackProximity: false,
        types: 'address, place, country, poi',
        reverseGeocode: true
    });

    geocoder.on('result', function(result) {
        console.log(TAG, 'Geocoder result', result);
        console.log(TAG, 'lastGeocoderLocation', result.result.place_name);
        //console.log(TAG, 'Geocoder element', $('#geocoder'));
        $('#latitude-input').val(result.result.center[1]);
        $('#longitude-input').val(result.result.center[0]);
        stash.lastGeocoderLocation = result.result.place_name;
        stash.geocoder.setTypes('address, place, country, poi');

        if (stash.sendReverseGeocodedMessage) {
            sendCreateMessageRequest(result.result.center[1],result.result.center[0]);
        }

    });

    stash.geocoder = geocoder;
         
    document.getElementById('geocoder').appendChild(geocoder.onAdd(map));

    /*
    geolocate.on('geolocate', function(e) {
        var lon = e.coords.longitude;
        var lat = e.coords.latitude
        var position = [lon, lat];
        console.log(TAG, 'geolocation',position);
    });
    */

    stash.map = map;
    stash.geolocate = geolocate;

    $('#navigation-sidebar-container').show();

    loadMapMessagesFromServer();
}

function setLoggedInInterface(afterSetCallback) {
    console.log(TAG, 'Called setLoggedInInterface');

    $('#map-write-button-section').show();
    $('#nav-login-button-container').hide();
    $('#nav-logout-button-container').show();
    $('#nav-profile-button-container').show();
    $('#nav-search-button-container').show();
    $('#nav-logged-icon-name').text(stash.name + ' ' + stash.surname);
    $('#nav-logged-icon-handle').text(stash.username);
    $('#nav-logged-icon-container').show();
    $('#login-error-div').hide();
    $('#nav-signup-button-container').hide();

    let imgString = stash.img ? 'data:image/jpeg;base64,' + stash.img : 'default_user_icon.png';
    $('#nav-logged-icon-image').attr('src', imgString);
    $('#create-message-image').attr('src', imgString);

    if (afterSetCallback) afterSetCallback();
}

function updateProfilePage() {
    console.log(TAG, 'Called updateProfilePage');

    $('#profile-page-name').text(stash.name + ' ' + stash.surname);
    $('#profile-page-handle').text('@' + stash.username);
    $('#profile-page-description').text(stash.description);

    let img = stash.img ? 'data:image/jpeg;base64,' + stash.img : 'default_user_icon.png';

    $('#profile-page-image').attr('src', img);
    $('#nav-logged-icon-image').attr('src', img);
    $('#create-message-image').attr('src', img);

    $('#profile-messages-section').find('.message-article').each( function(index) {
        //console.log('found ', index);
        let handle = $(this).find('.message-handle').text();
        //console.log('handle', handle);

        if (handle === '@' + stash.username) {
            $(this).find('.message-image').attr('src', img);
            $(this).find('.message-name-surname').text(stash.name + ' ' + stash.surname);
        }
    });
}

function getSortAndFilterObject() {
    console.log(TAG, 'Called getSortAndFilterObject');

    let sortKey = $('#map-coord-sort-key-input').val();
    let sortOrder = $('input[name=sort-order-radios]:checked', '#sort-order-input-container').val();

    let df = $('#date-from-input').val();
    let tf = $('#time-from-input').val();
    let dt = $('#date-to-input').val();
    let tt = $('#time-to-input').val();

    console.log(TAG, 'only liked', $('#only-liked-input').prop('checked'));

    console.log(TAG, 'filter', df, tf, dt, tt);
    console.log(TAG, 'from', df + ' ' + tf, moment(df + ' ' + tf, 'YYYY-MM-DD HH:mm').format(), moment(df + ' ' + tf, 'YYYY-MM-DD HH:mm').unix());
    console.log(TAG, 'to', dt + ' ' + tt, moment(tf + ' ' + tt, 'YYYY-MM-DD HH:mm').format(), moment(dt + ' ' + tt, 'YYYY-MM-DD HH:mm').unix());

    return { sortKey: sortKey, sortOrder: sortOrder, onlyLiked: $('#only-liked-input').prop('checked'), timeFrom: moment(df + ' ' + tf, 'YYYY-MM-DD HH:mm').unix(), timeTo: moment(dt + ' ' + tt, 'YYYY-MM-DD HH:mm').unix() }
}

function setOnOpenFilter() {
    $('#filter-icon-container').one('click', function() {
        console.log(TAG, 'Clicked filter-icon-container while closed');

        $(this).find('svg').remove();

        let newSvg =
            `
                <svg style='width: 25px; height: 25px; color: rgb(29, 161, 242);' width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-filter-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zM3.5 5a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1h-9zM5 8.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm2 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5z"/>
                </svg>
            `;

        $(this).html($(this).html() + newSvg);

        $('#filter-container').show();

        setOnCloseFilter();
    });
}

function setOnCloseFilter() {
    $('#filter-icon-container').one('click', function() {
        console.log(TAG, 'Clicked filter-icon-container while open');

        $(this).find('svg').remove();

        let newSvg =
            `
                <svg style='width: 25px; height: 25px' width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-filter-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                    <path fill-rule="evenodd" d="M7 11.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5z"/>
                </svg>
            `;

        $(this).html($(this).html() + newSvg);

        $('#filter-container').hide();

        setOnOpenFilter();
    });
}

function showAlert(alertString) {
    let alert = $('#global-alert');
    alert.text(alertString);
    alert.fadeIn(500);
    setTimeout(function() { 
       alert.fadeOut(500); 
    }, 3000);
} 

export {showPage, setLoggedInInterface, updateProfilePage, getSortAndFilterObject, 
    setOnOpenFilter, setOnCloseFilter, loadMapMessagesAndShowMap, loadMap, showAlert };