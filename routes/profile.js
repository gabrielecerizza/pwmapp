var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST profile */
router.post('/', function(req, res, next) {
    console.log('Received profile request: ' + JSON.stringify(req.body));
    let result = {};
    db.one({
        text: 'WITH following_table AS (\
            SELECT following_user_id, COUNT(*) AS following_count\
            FROM follow\
            WHERE following_user_id = $1\
            GROUP BY following_user_id\
        ),\
        followed_table AS (\
            SELECT followed_user_id, COUNT(*) AS followed_count\
            FROM follow\
            WHERE followed_user_id = $1\
            GROUP BY followed_user_id\
        )\
        SELECT user_id, username, name, surname, img_version, description, coalesce(following_count, 0) AS following_count, coalesce(followed_count, 0) AS followed_count, encode(img::bytea, \'base64\') AS img\
        FROM following_table FULL JOIN followed_table ON following_user_id = followed_user_id\
            RIGHT JOIN registered_user ON following_user_id = user_id\
        WHERE user_id = $1',
        values: [req.body.queried_user_id]
        })
        .then(data => {
            //console.log('Profile first success', data);
            result = {user_data: data};
            console.log('Profile first success, sending:', result);

            // Need to stay here, otherwise it may fire before user_data
            db.any({
                text: 'WITH liked_num AS (\
                        SELECT message_id, COUNT(*)\
                        FROM liked\
                        GROUP BY message_id\
                    ),\
                    liking(message_id) AS (\
                        SELECT message_id\
                        FROM liked\
                        WHERE user_id = $1\
                    )\
                    SELECT m.user_id, m.message_id, CASE WHEN l.message_id IS NULL THEN false ELSE true END AS liked, latitude, longitude, message, time, location, coalesce(liked_num.count, 0) AS like_count\
                    FROM message AS m LEFT JOIN liking AS l ON m.message_id = l.message_id LEFT JOIN liked_num ON m.message_id = liked_num.message_id\
                    WHERE m.user_id = $1 ORDER BY time DESC',
                values: [req.body.queried_user_id]
                })
                .then(data => {
                    result = { ...result, messages: data};
                    console.log('Profile second success, sending:', result);
                    res.json(result);
                })
                .catch(error => {
                    console.log('ERROR:', error);
                    res.status(400).send(error);
                });
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;