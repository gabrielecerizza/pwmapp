-- \du to show user list
-- \dt show tables
-- \l to show db list
-- \h for help
-- \q to quit
-- psql pwmdb

CREATE ROLE gabriele;
ALTER ROLE gabriele WITH LOGIN;
ALTER ROLE gabriele WITH CREATEDB;
ALTER ROLE gabriele WITH PASSWORD 'pwmpwd';
ALTER ROLE gabriele SET search_path TO 'pwmschema';

CREATE SCHEMA pwmschema;
SET search_path TO pwmschema;