var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST create_message */
router.post('/', function(req, res, next) {
    console.log('Received create_message request: ' + JSON.stringify(req.body));
    db.none({
        text: 'INSERT INTO message(user_id, latitude, longitude, message, time, location) VALUES ($1, $2, $3, $4, $5, $6)',
        values: [req.body.user_id, req.body.latitude, req.body.longitude, req.body.message, new Date().toUTCString(), req.body.location]
        })
        .then(() => {
            console.log('Inserted message');
            res.sendStatus(200);
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;