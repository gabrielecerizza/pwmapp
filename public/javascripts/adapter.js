import { sendFollowRequest, sendUnfollowRequest, sendUnlikeRequest, sendLikeRequest } from './server_calls';
import { stash } from './stash';
import { loadMapMessagesAndShowMap } from './utility';
const TAG = 'adapter:';

class Adapter {
    constructor(div, objectArray) {
        this._div = div;
        this._objectArray = objectArray;
        console.log(objectArray);
    }

    get objectArray() {
        return this._objectArray;
    }

    refresh() {
        $('#'+this._div).html('');
        $('#'+this._div).html('<ul id="adapterUl" class="list-group" style="width: 90%"></ul>');
        for (let i = 0; i < this._objectArray.length; i++) {
            this.generateListElement(i, this._objectArray[i]);
        }
    }

    generateListElement(index, element) {
        $('#adapterUl').append('<li class="list-group-item">' + element.toString() + '</li>');
    }
}

class MessagesAdapter {
    constructor(container, users, messages, isUnregisteredUser, onlyLiked, onlyFollowed) {
        this._container = container;
        this._users = users;
        this._messages = messages;
        this._isUnregisteredUser = isUnregisteredUser;
        this._filteredMessages = messages;
        this._onlyLiked = onlyLiked;
        this._onlyFollowed = onlyFollowed;
    }

    get messages() {
        return this._messages;
    }

    get users() {
        return this._users;
    }

    get filteredMessages() {
        return this._filteredMessages;
    }

    set filteredMessages(value) {
        this._filteredMessages = value;
    }

    get isUnregisteredUser() {
        return this._isUnregisteredUser;
    }

    get onlyLiked() {
        return this._onlyLiked;
    }

    get onlyFollowed() {
        return this._onlyFollowed;
    }

    refresh(sortAndFilterObject) {
        $('.message-icon-container').off();
        // Needed to avoid problems when opening profile after having opened map coord modal
        $('.message-article').remove();
        $('body').off(); // Needed to avoid firing duplicate events
        $(this._container).html('');
        
        this.filteredMessages = this.messages;

        if (sortAndFilterObject) {
            if (sortAndFilterObject.timeFrom) {
                this.filteredMessages = this.filteredMessages.filter( function(value) {
                    console.log('time to be filtered from:', value.time, moment(value.time, 'dddd, MMMM Do YYYY, h:mm:ss a').unix());
                    return moment(value.time).unix() >= sortAndFilterObject.timeFrom;
                });
            }
    
            if (sortAndFilterObject.timeTo) {
                this.filteredMessages = this.filteredMessages.filter( function(value) {
                    console.log('time to be filtered to:', value.time, moment(value.time, 'dddd, MMMM Do YYYY, h:mm:ss a').unix());
                    return moment(value.time).unix() <= sortAndFilterObject.timeTo;
                });
            }
            
            if (sortAndFilterObject.onlyLiked) {
                this.filteredMessages = this.filteredMessages.filter( function(value) {
                    return value.liked;
                });
            }

            this.filteredMessages.sort(function(a,b) {
                switch (sortAndFilterObject.sortKey) {
                    case 'time':
                        if (sortAndFilterObject.sortOrder === 'asc') {
                            return moment(a.time).unix() - moment(b.time).unix();
                        } else {
                            return moment(b.time).unix() - moment(a.time).unix();
                        }
                    case 'like':
                        if (sortAndFilterObject.sortOrder === 'asc') {
                            return a.like_count - b.like_count;
                        } else {
                            return b.like_count - a.like_count;
                        }
                }
            });
        }

        // Needed for liked messages in profile page (don't want to rely on sortAndFilterObject)
        if (this.onlyLiked) {
            this.filteredMessages = this.filteredMessages.filter( function(value) {
                return value.liked;
            });
        }

        // Needed for messages from followed tab in profile page
        if (this.onlyFollowed) {
            this.filteredMessages = this.filteredMessages.filter( function(element) {
                function searchUser (value, index, array) {
                    return value.user_id === element.user_id;
                }
                let user = stash.adapter.users.find(searchUser);
            
                return user.followed;
            });
        }

        for (let i = 0; i < this.filteredMessages.length; i++) {
            this.generateListElement(i, this.filteredMessages[i]);
        }
        $('.message-icon-container').hover(function() {
            $(this).find('.message-halo-div').css('background-color', 'rgba(224, 36, 94, 0.1)');
        }, function() {
            $(this).find('.message-halo-div').css('background-color', 'rgba(224, 36, 94, 0)');
        });

        $('[data-toggle="popover"]').popover();
            $(".pop").popover({trigger: "hover", html: true, container: 'body'})
                .on("mouseenter", function() {
                    //console.log('mouseenter');
                    var _this = this;
                    $(this).popover("show");
                    $(".popover").on("mouseleave", function() {
                        $(_this).popover('hide');
                    });
                })
                .on("mouseleave", function() {
                    //console.log('mouseleave');
                    var _this = this;
                    setTimeout(function() {
                        if (!$(".popover:hover").length) {
                            $(_this).popover("hide");
                        }
                    }, 200);
                });
    }

    refreshPopovers() {
        console.log('Refresh popovers', this.filteredMessages, this._container);

        for (let i = 0; i < this.filteredMessages.length; i++) {
            let element = this.filteredMessages[i];
            console.log('Followed: ', i, element.followed);
            $('#a' + element.message_id).attr('data-content', this.getPopoverHtml(i, element));
        }
        
    }

    getPopoverHtml(index, element) {
        let follow_id = 'follow' + element.message_id;
        function searchUser (value, index, array) {
            return value.user_id === element.user_id;
        }

        let user = this.users.find(searchUser);

        let followButtonHtml =
            `
                <div id='${follow_id}' class='d-flex flex-column justify-content-center rounded-left-right ${ (user.followed) ? "follow-button-following": "follow-button" }' >
                    <span>
                        ${ (user.followed) ? 'Following': 'Follow' }
                    </span>
                </div>
            `;

        // Need to use css classes because popover ignores style
        let popoverHtml = 
            `
                <div id='${ 'popover' + element.message_id }' class='d-flex flex-column popover-container'>
                    <!-- Image and Follow -->
                    <div class='d-flex justify-content-between'>
                        <img class='rounded-circle popover-image' alt='user image'>
                        ${ (stash.userId != element.user_id && !this.isUnregisteredUser) ? followButtonHtml : ''}
                    </div>
                    <!-- Name and Handle -->
                    <div class='popover-name-container'>
                        <div class='black-bold-text'>
                            <span >
                                ${ user.name + ' ' + user.surname }
                            </span>
                        </div>
                        <div class='gray-text'>
                            <span>
                                @${ user.username }
                            </span>
                        </div>
                    </div>
                    <!-- Description -->
                    <div class='popover-description-container d-flex jusitfy-content-between'>
                        <span>
                            ${ user.description }
                        </span>
                    </div>
                    <!-- Follow Numbers -->
                    <div class='popover-follow-container d-flex justify-content-between'>
                        <div class='d-flex' >
                            <div class='popover-follow-numbers-divider'>
                                <span class='black-bold-text'>
                                    ${ user.following_count }
                                </span>
                                <span class='gray-text'>
                                    Following
                                </span>
                            </div>
                            <div>
                                <span class='black-bold-text'>
                                    ${ user.followed_count }
                                </span>
                                <span class='gray-text'>
                                    Followers
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            `;

        return popoverHtml;
    }

    generateListElement(index, element) {

        let like_id = 'like' + element.message_id;
        let a_id = 'a' + element.message_id;
        let follow_id = 'follow' + element.message_id;
        let location_id = 'location' + element.message_id;

        function searchUser (value, index, array) {
            return value.user_id === element.user_id;
        }

        let user = this.users.find(searchUser);
        let imgString = user.img ? 'data:image/jpeg;base64,' + user.img : 'default_user_icon.png';
        let time = moment(element.time).format('dddd, MMMM Do YYYY, h:mm:ss a');

        let likedIconContainer = 
            `
                <div class='d-inline-flex' style='position: relative'>
                    <div class='message-halo-div rounded-circle'></div>
                    <svg style='color: rgb(224, 36, 94);' width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-heart-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                    </svg>
                </div>
            `;
        
        let unlikedIconContainer = 
            `
                <div class='d-inline-flex' style='position: relative'>
                    <div class='message-halo-div rounded-circle'></div>
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-heart" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 2.748l-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                    </svg>
                </div>
            `;

        let likedCountSpan = 
            `
                <span style='color: rgb(224, 36, 94);' class='message-count-span'>${ (element.like_count > 0) ? Number(element.like_count) : '' }</span>
            `;

        let unlikedCountSpan =
            `
                <span class='message-count-span'>${ (element.like_count > 0) ? Number(element.like_count) : '' }</span>
            `;

        let messageArticle = 
            `
                <article class='message-article'>
                    <!-- Actual element -->
                    <div class='d-flex'>
                        <!-- Image -->
                        <div class='flex-column message-image-container'>
                            <div style='width: 50px; height: 50px'>
                                <img class='message-image rounded-circle' src="${ imgString }" alt='user image'>
                            </div>
                        </div>
                        <!-- Text -->
                        <div class='d-flex flex-column w-100' style='padding-bottom: 10px'>
                            <!-- Name, Handler and Date -->
                            <div class='d-flex justify-content-between w-100' style='margin-bottom: 2px;'>
                                <div>
                                    <span class='black-bold-text'>
                                        <a id="${ a_id }" class="pop black-bold-text message-name-surname" data-toggle="popover" data-placement="bottom" data-html="true" data-content="${ this.getPopoverHtml(index, element)}" >  
                                            ${ user.name + ' ' + user.surname }
                                        </a>
                                    </span>
                                    <span class='gray-text message-handle' style='margin-left: 5px'>@${ user.username }</span>
                                </div>
                                <div>  
                                    <time class='gray-text' datetime='${time}'>${time}</time>
                                </div>
                            </div>
                            <!-- Location -->
                            <div class='d-flex'>
                               <a id='${ location_id }' class='gray-text location-link'>${element.location}</a>
                            </div>
                            <!-- Text and Buttons -->
                            <div class ='d-flex flex-column'>
                                <span>${element.message}</span>
                                <div class='d-flex' style='margin-top: 10px'>
                                    <div id='${ like_id }' class='message-icon-container d-flex align-items-center'>
                                        ${ element.liked ? likedIconContainer : unlikedIconContainer }
                                        ${ element.liked ? likedCountSpan : unlikedCountSpan }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            `;
        
        $(this._container).append(messageArticle);

        // Doesn't work any other way. Can't place image on show rather than shown
        $('body').on('shown.bs.popover', '#' + a_id, function() {
            console.log(TAG, 'shown bs popover');
            $('body').find('.popover-image').attr('src', imgString);
        })

        // Need to attach events to body and use delegate method because popover does not exist yet
        $('body').on('click', '#' + follow_id + '.follow-button', function() {
            console.log('clicked on popover follow', JSON.stringify(element));
            sendFollowRequest(element);
        });

        $('body').on('click', '#' + follow_id + '.follow-button-following', function() {
            console.log('clicked on popover unfollow', JSON.stringify(element));
            sendUnfollowRequest(element);
        });
        
        $('body').on('mouseenter', '#' + follow_id + '.follow-button-following', function() {
            setTimeout(() => $('#' + follow_id).html('Unfollow'), 100);
        });

        $('body').on('mouseleave', '#' + follow_id + '.follow-button-following', function() {
            setTimeout(() => $('#' + follow_id).html('Following'), 100);
        });

        $('body').on('click', '#' + like_id, function() {
            // Need to use stash because here "this" is the clicked icon
            if (stash.adapter.isUnregisteredUser) {
                console.log(TAG, 'Clicked on like-unlike as unregistered user', like_id);
            } else if (element.liked) {
                console.log(TAG, 'Clicked on unlike', like_id);
                sendUnlikeRequest(element);
            } else if (!element.liked) {
                console.log(TAG, 'Clicked on like', like_id);
                sendLikeRequest(element);
            }
        });

        $('body').on('click', '#' + location_id, function() {
            console.log(TAG, 'Clicked location link', location_id);
            $('.modal').modal('hide');

            function callback() {
                stash.map.flyTo({
                    center: [element.longitude, element.latitude],
                    zoom: 15,
                    essential: true // this animation is considered essential with respect to prefers-reduced-motion
                    });
            }

            loadMapMessagesAndShowMap(callback);
        })
    }

    pruneUnfollowed(message) {
         if (this.onlyFollowed) {
            for (let i = 0; i < this.filteredMessages.length; i++) {
                let element = this.filteredMessages[i];
                if (element.user_id == message.user_id) {
                    $('#a' + element.message_id).closest('.message-article').remove();
                }
            }
            $('.popover').remove();
         } else {
            // Needed to instantly refresh the popover currently on display
            // Happens only when not on profile followed page
            $('#a' + message.message_id).popover('show');
         }

    }
}

class UsersAdapter {
    constructor(container, users) {
        this._container = container;
        this._users = users;
    }

    get users() {
        return this._users;
    }

    refresh() {
        $(this._container).html('');

        for (let i = 0; i < this.users.length; i++) {
            this.generateListElement(i, this.users[i]);
        }
    }

    generateListElement(index, user) {

        let follow_id = 'search-follow' + user.user_id;
        let imgString = user.img ? 'data:image/jpeg;base64,' + user.img : 'default_user_icon.png';

        let followButtonHtml =
            `
                <div id='${follow_id}' class='d-flex flex-column justify-content-center rounded-left-right ${ (user.followed) ? "follow-button-following": "follow-button" }' >
                    <span>
                        ${ (user.followed) ? 'Following': 'Follow' }
                    </span>
                </div>
            `;

        let userArticle = 
            `
                <article class='search-user-article'>
                    <div class='d-flex flex-column search-user-container'>
                        <!-- Image and Follow -->
                        <div class='d-flex justify-content-between'>
                            <img class='rounded-circle search-user-image' src='${ imgString }' alt='user image'>
                            ${ (stash.userId != user.user_id) ? followButtonHtml : ''}
                        </div>
                        <!-- Name and Handle -->
                        <div class='search-user-name-container'>
                            <div class='black-bold-text'>
                                <span >
                                    ${ user.name + ' ' + user.surname }
                                </span>
                            </div>
                            <div class='gray-text'>
                                <span>
                                    @${ user.username }
                                </span>
                            </div>
                        </div>
                        <!-- Description -->
                        <div class='popover-description-container d-flex jusitfy-content-between'>
                            <span>
                                ${ user.description }
                            </span>
                        </div>
                        <!-- Follow Numbers -->
                        <div class='search-user-follow-container d-flex justify-content-between'>
                            <div class='d-flex' >
                                <div class='search-user-follow-numbers-divider'>
                                    <span class='black-bold-text'>
                                        ${ user.following_count }
                                    </span>
                                    <span class='gray-text'>
                                        Following
                                    </span>
                                </div>
                                <div>
                                    <span class='black-bold-text'>
                                        ${ user.followed_count }
                                    </span>
                                    <span class='gray-text'>
                                        Followers
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            `;

        $(this._container).append(userArticle);

        $('#' + follow_id + '.follow-button').on('click', function() {
            console.log(TAG, 'Clicked on search user follow');
            sendFollowRequest(user);
        });

        $('#' + follow_id + '.follow-button-following').on('click', function() {
            console.log(TAG, 'Clicked on search user unfollow');
            sendUnfollowRequest(user);
        });
        
        $('#' + follow_id + '.follow-button-following').on('mouseenter', function() {
            setTimeout(() => $('#' + follow_id).html('Unfollow'), 100);
        });

        $('#' + follow_id + '.follow-button-following').on('mouseleave', function() {
            setTimeout(() => $('#' + follow_id).html('Following'), 100);
        });
    }
}

export { MessagesAdapter, UsersAdapter };