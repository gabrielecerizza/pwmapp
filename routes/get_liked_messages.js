var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST get_liked_messages */
router.post('/', function(req, res, next) {
    console.log('Received get_liked_messages request: ' + JSON.stringify(req.body));
    let result = {};
    db.any({
        text: 'WITH following_table AS (\
            SELECT following_user_id, COUNT(*) AS following_count\
            FROM follow\
            GROUP BY following_user_id\
        ),\
        followed_table AS (\
            SELECT followed_user_id, COUNT(*) AS followed_count\
            FROM follow\
            GROUP BY followed_user_id\
        ),\
        liked_messages_users AS (\
            SELECT DISTINCT(m.user_id)\
            FROM liked AS l JOIN message AS m ON l.message_id = m.message_id\
            WHERE l.user_id = $1\
        ),\
        followed_by_user(followed_by_user_id) AS (\
            SELECT followed_user_id\
            FROM follow\
            WHERE following_user_id = $1\
        )\
        SELECT r.user_id, username, name, surname, description, CASE WHEN followed_by_user_id IS NULL THEN false ELSE true END AS followed, coalesce(following_count, 0) AS following_count, coalesce(followed_count, 0) AS followed_count, img_version, encode(img::bytea, \'base64\') AS img\
        FROM registered_user AS r JOIN liked_messages_users AS l ON r.user_id = l.user_id LEFT JOIN following_table ON r.user_id = following_user_id\
            LEFT JOIN followed_table ON r.user_id = followed_user_id LEFT JOIN followed_by_user ON r.user_id = followed_by_user_id',
        values: [req.body.user_id] 
        })
        .then(data => {
            console.log('get_liked_messages first success', data);
            result = { liked_messages_users: data };

            db.any({
                text: 'WITH liked_num AS (\
                    SELECT message_id, COUNT(*)\
                    FROM liked\
                    GROUP BY message_id\
                )\
                SELECT m.user_id, m.message_id, message, latitude, longitude, location, time, true AS liked, coalesce(l.count, 0) AS like_count\
                FROM message AS m JOIN liked ON m.message_id = liked.message_id LEFT JOIN liked_num AS l ON m.message_id = l.message_id\
                WHERE liked.user_id = $1\
                ORDER BY time DESC',
                values: [req.body.user_id]
                })
                .then(data => {
                    console.log('get_liked_messages second success', data);
                    result = { ...result, messages: data};
                    res.json(result);
                })
                .catch(error => {
                    console.log('ERROR:', error);
                    res.status(400).send(error);
                });
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;