class Stash {
    constructor() {
        this._userId = undefined;
        this._username = undefined;
        this._name = undefined;
        this._surname = undefined;
        this._description = undefined;
        this._map = undefined;
        this._geolocate = undefined;
        this._geocoder = undefined;
        this._markers = [];
        this._adapter = {};
        this._lastGeocoderLocation = undefined;
        this._sendReverseGeocodedMessage = false; //needed to handle asynchronous reverse geocoding
    }

    get user_id() {
        return this._user_id;
    }

    set user_id(value) {
        this._user_id = value;
    }

    get username() {
        return this._username;
    }

    set username(value) {
        this._username = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get surname() {
        return this._surname;
    }

    set surname(value) {
        this._surname = value;
    }

    get description() {
        return this._description;
    }

    set description(value) {
        this._description = value;
    }

    get lastGeocoderLocation() {
        return this._lastGeocoderLocation;
    }

    set lastGeocoderLocation(value) {
        this._lastGeocoderLocation = value;
    }

    get map() {
        return this._map;
    }

    set map(value) {
        this._map = value;
    }

    get markers() {
        return this._markers;
    }

    set markers(value) {
        this._markers = value;
    }

    get geolocate() {
        return this._geolocate;
    }

    set geolocate(value) {
        this._geolocate = value;
    }

    get geocoder() {
        return this._geocoder;
    }

    set geocoder(value) {
        this._geocoder = value;
    }

    get adapter() {
        return this._adapter;
    }

    set adapter(value) {
        this._adapter = value;
    }
}

let stash = new Stash();

export { stash };