var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST unlike */
router.post('/', function(req, res, next) {
    console.log('Received unlike request: ' + JSON.stringify(req.body));
    db.none({
        text: 'DELETE FROM liked WHERE user_id = $1 AND message_id = $2',
        values: [req.body.user_id, req.body.message_id]
        })
        .then(data => {
            console.log('Unlike success');
            res.sendStatus(200);
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;