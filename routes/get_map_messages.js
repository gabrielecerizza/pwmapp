var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* GET get_map_messages */
router.get('/', function(req, res, next) {
    console.log('Received get_map_messages request: ' + JSON.stringify(req.body));
    db.any('SELECT latitude, longitude, COUNT(*) FROM message GROUP BY latitude, longitude')
        .then(data => {
            console.log('Get map messages success');
            res.json({ messages: data });
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;