var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST like */
router.post('/', function(req, res, next) {
    console.log('Received like request: ' + JSON.stringify(req.body));
    db.none({
        text: 'INSERT INTO liked VALUES ($1, $2)',
        values: [req.body.user_id, req.body.message_id]
        })
        .then(data => {
            console.log('Like success');
            res.sendStatus(200);
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;