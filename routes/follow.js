var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST follow */
router.post('/', function(req, res, next) {
    console.log('Received follow request: ' + JSON.stringify(req.body));
    db.one({
        text: 'SELECT * FROM follow($1, $2)',
        values: [req.body.user_id, req.body.other_user_id]
        })
        .then(data => {
            console.log('Follow success');
            res.sendStatus(200);
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;