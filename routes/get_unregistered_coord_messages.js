var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST get_unregistered_coord_messages */
router.post('/', function(req, res, next) {
    console.log('Received get_unregistered_coord_messages request: ' + JSON.stringify(req.body));
    let result = {};
    db.any({
        text: 'WITH mess_users AS (\
                SELECT r.user_id\
                FROM registered_user AS r JOIN message AS m ON r.user_id = m.user_id\
                WHERE ABS(latitude - $1) < 0.00001 AND ABS(longitude - $2) < 0.00001\
                GROUP BY r.user_id\
            ),\
            following_table AS (\
                SELECT following_user_id, COUNT(*) AS following_count\
                FROM follow\
                GROUP BY following_user_id\
            ),\
            followed_table AS (\
                SELECT followed_user_id, COUNT(*) AS followed_count\
                FROM follow\
                GROUP BY followed_user_id\
            )\
            SELECT r.user_id, username, name, surname, description, coalesce(following_count, 0) AS following_count, coalesce(followed_count, 0) AS followed_count, img_version, encode(img::bytea, \'base64\') AS img\
            FROM registered_user AS r JOIN mess_users AS m ON r.user_id = m.user_id\
            LEFT JOIN following_table ON r.user_id = following_user_id LEFT JOIN followed_table ON r.user_id = followed_user_id',
        values: [req.body.latitude, req.body.longitude]
        })
        .then(data => {
            console.log('get_unregistered_coord_messages first success', JSON.stringify(data));
            result = { users: data };
            
            db.any({
                text:'WITH liked_num AS (\
                    SELECT message_id, COUNT(*)\
                    FROM liked\
                    GROUP BY message_id\
                )\
                SELECT r.user_id, m.message_id, message, latitude, longitude, location, time, coalesce(l.count, 0) AS like_count\
                FROM registered_user AS r JOIN message AS m ON r.user_id = m.user_id LEFT JOIN liked_num AS l ON m.message_id = l.message_id\
                WHERE ABS(latitude - $1) < 0.00001 AND ABS(longitude - $2) < 0.00001\
                ORDER BY time DESC',
                values: [req.body.latitude, req.body.longitude]
                })
                .then(data => {
                    console.log('get_unregistered_coord_messages second success', JSON.stringify(data));
                    result = { ...result, messages: data };
                    res.json(result);
                })
                .catch(error => {
                    console.log('ERROR:', error);
                    res.status(400).send(error);
                });
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;