var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST login */
router.post('/', function(req, res, next) {
    console.log('Received login request: ' + JSON.stringify(req.body));
    db.one({
        text: 'SELECT user_id, username, name, surname, encode(img::bytea, \'base64\') AS img, img_version, description FROM registered_user WHERE username = $1 AND password = $2',
        values: [req.body.username, req.body.password]
        })
        .then(data => {
            console.log('Login result:', data);
            res.json(data);
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send({ error: 'User not found' });
        });
});

module.exports = router;