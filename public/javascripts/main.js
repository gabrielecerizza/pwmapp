import { sendCreateMessageRequest, sendLoginRequest, sendRegisterRequest,
            sendProfileRequest, sendGetUserDataRequest, sendProfileUpdateRequest,
            sendGetUserMessagesRequest, sendGetFollowedMessagesRequest, sendGetLikedMessagesRequest,
            sendSearchRequest } from './server_calls';
import { stash } from './stash';
import { showPage, getSortAndFilterObject, setOnOpenFilter, loadMapMessagesAndShowMap, 
    loadMap, showAlert } from './utility';
const TAG = 'main:';

$(function(){
    console.log(TAG, 'Document ready');
    bindEvents();
    userIdLogic();
});

function bindEvents() {
    console.log(TAG, 'Called bindEvents');
    bindNavbarEvents();
    bindMapWriteButtonEvents();
    bindMessageInputGroupEvents();
    bindLoginEvents();
    bindLogoutEvents();
    bindInputEvents();
    bindSignupEvents();
    bindProfileEvents();
    bindEditProfileEvents();
    bindSortFilterEvents();
    bindSearchEvents();
}

function userIdLogic() {
    console.log(TAG, 'Called userIdLogic');
    let userId = localStorage.getItem('userId');
    if (userId) {
        stash.userId = userId;
        sendGetUserDataRequest();
    } else {
        loadMap();
    }
}

function bindNavbarEvents() {
    console.log(TAG, 'Called bindNavbarEvents');

    $('#navbar-map-button').css('color', 'rgb(29, 161, 242)');

    $('.nav-button').on('click', function() {
        $('.nav-button').css('color', 'rgb(20, 23, 26)');
        $(this).css('color', 'rgb(29, 161, 242)');
    });

    $('#navbar-map-button').on('click', function() {
        $('#global-alert-container').css('max-width', '100%');
        loadMapMessagesAndShowMap();
    });

    $('#navbar-login-button').on('click', function() {
        showPage('.login-page');
        $('#login-submit-button').show();
    });

    $('#navbar-search-button').on('click', function() {
        showPage('.search-page');
    });

    $('#navbar-profile-button').on('click', function() {
        $('.profile-navbar-button-div').css('color', 'rgb(101, 119, 134)');
        $('.profile-navbar-button-div').css('border-bottom-color', 'rgba(0, 0, 0, 0)');
        $('#profile-navbar-my-messages').css('color', 'rgb(29, 161, 242)');
        $('#profile-navbar-my-messages').css('border-color', 'rgb(29, 161, 242)');
        sendProfileRequest();
    });
}

function bindMapWriteButtonEvents() {
    console.log(TAG, 'Called bindMapWriteButtonEvents');
    let mapWriteButtonSvgContainer = $('#map-write-button-svg-container');
    $('#map-write-button').hover(
        function mouseenter() {
            mapWriteButtonSvgContainer.addClass('map-write-button-svg-container-hover');
            $('#map-write-button-message').show();
        },
        function mouseleave() {
            mapWriteButtonSvgContainer.removeClass('map-write-button-svg-container-hover');
            $('#map-write-button-message').hide();
        }
    );

    let mapCloseWriteButtonSvgContainer = $('#map-close-write-button-svg-container');
    $('#map-close-write-button').hover(
        function mouseenter() {
            mapCloseWriteButtonSvgContainer.addClass('map-close-write-button-svg-container-hover');
            $('#map-close-write-button-message').show();
        },
        function mouseleave() {
            mapCloseWriteButtonSvgContainer.removeClass('map-close-write-button-svg-container-hover');
            $('#map-close-write-button-message').hide();
        }
    );

    mapWriteButtonSvgContainer.on('click', function() {
        console.log(TAG, 'Clicked on map-write-button-svg-container');
        $('#map-write-input-section').show();
        $('#map-write-button').hide();
        $('#map-close-write-button').show();
    });

    mapCloseWriteButtonSvgContainer.on('click', function() {
        console.log(TAG, 'Clicked on map-close-write-button-svg-container');
        $('#map-write-input-section').hide();
        $('#map-write-button').show();
        $('#map-close-write-button').hide();
        stash.geocoder.clear();
        $('#message-textarea').val('');
        $('#latitude-input').val('');
        $('#longitude-input').val('');
    });
}

function bindMessageInputGroupEvents() {
    console.log(TAG, 'Called bindMessageInputGroupEvents');

    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });

    let tracking = true;

    let createMessageButton = $('#create-message-button');
    let messageCustomCoordIcon = $('#message-custom-coord-icon');
    let messageTrackCoordIcon = $('#message-track-coord-icon');
    let messageCoordInputGroup = $('#message-coord-input-group');
    messageCoordInputGroup.hide();

    messageCustomCoordIcon.on('click', function() {
        console.log(TAG, 'message-custom-coord-icon clicked');
        $(this).addClass('icon-clicked');
        messageTrackCoordIcon.removeClass('icon-clicked');
        messageCoordInputGroup.show();
        tracking = false;
    });

    messageTrackCoordIcon.on('click', function() {
        console.log(TAG, 'message-track-coord-icon clicked');
        $(this).addClass('icon-clicked');
        messageCustomCoordIcon.removeClass('icon-clicked');
        messageCoordInputGroup.hide();
        tracking = true;
    });

    createMessageButton.on('click', function() {
        console.log(TAG, 'create_message_button clicked');
        prepareCreateMessageRequest(tracking);
    });
}

function bindLoginEvents() {
    console.log(TAG, 'Called bindLoginEvents');

    $('.login-input-container').focusin(function() {
        $(this).find('.login-input-header').css( 'color', 'rgb(29, 161, 242)');
        $(this).find('.login-label').css( 'border-color', 'rgb(29, 161, 242)');
    });
    $('.login-input-container').focusout(function() {
        $(this).find('.login-input-header').css( 'color', 'rgb(101, 119, 134)');
        $(this).find('.login-label').css( 'border-color', 'rgb(101, 119, 134)');
    });
    
    $('.login-input').on('input',function(){
        if ($('#login-password-input').val() !== '' && $('#login-username-input').val() !== ''){
            $('#login-submit-button-container').find('button').prop('disabled',false);
        } else {
            $('#login-submit-button-container').find('button').prop('disabled',true);
        }
    });

    $('#login-submit-button').on('click', function() {
        console.log(TAG, 'Clicked on login submit button');
        sendLoginRequest();
        $('#login-submit-button-container').find('button').prop('disabled',true);
    });
}

function prepareCreateMessageRequest(tracking) {
    console.log(TAG, 'Called prepareCreateMessageRequest', 'tracking: ' + tracking);

    let lat = 45.47474;
    let lon = 9.17984;

    if (tracking) {
        var options = {
            enableHighAccuracy: true,
            timeout: 100,
            maximumAge: 0
        };
        
        function success(pos) {
            let crd = pos.coords;
            lat = crd.latitude;
            lon = crd.longitude;
            stash.sendReverseGeocodedMessage = true;
            stash.geocoder.setTypes('address');

            console.log(TAG, 'Navigator', `Latitude: ${crd.latitude}`, `Longitude: ${crd.longitude}`);
            console.log(TAG, 'Reverse geocode result:', stash.geocoder.query(lat + ',' + lon));
        }
        
        function error(err) {
            console.warn(`ERROR(${err.code}): ${err.message}`);
            console.log(TAG, 'Error from navigator');

            let userlocation = stash.geolocate._lastKnownPosition;

            try {
                let lat = userlocation.coords.latitude;
                let lon = userlocation.coords.longitude;
                stash.sendReverseGeocodedMessage = true;
                stash.geocoder.setTypes('address');

                console.log(TAG, 'Mapbox geolocator', `Latitude: ${lat}`, `Longitude: ${lon}`);
                console.log(TAG, 'Reverse geocode result:', stash.geocoder.query(lat + ',' + lon));

            } catch (error) {
                console.log(TAG, 'Unable to locate the user', JSON.stringify(error));
                
                $('#map-write-input-section').hide();
                $('#map-write-button').show();
                $('#map-close-write-button').hide();

                showAlert('Error: unable to locate the user');
            }
        }

        navigator.geolocation.getCurrentPosition(success, error, options);
    } else {
        // Creating message with custom coordinates
        sendCreateMessageRequest($('#latitude-input').val(), $('#longitude-input').val());
    }
}

function bindLogoutEvents() {
    console.log(TAG, 'Called bindLogoutEvents');
    $('#logout-button').on('click', function() {
        stash.userId = undefined;
        stash.username = undefined;
        stash.name = undefined;
        stash.surname = undefined;
        stash.adapter = undefined;
        localStorage.setItem('userId', '');
        $('#nav-logout-button-container').hide();
        $('#nav-login-button-container').show();
        $('#nav-logged-icon-name').text('');
        $('#nav-logged-icon-handle').text('');
        $('#nav-logged-icon-container').hide();
        $('#nav-signup-button-container').show();
        $('#nav-profile-button-container').hide();
        $('#nav-search-button-container').hide();
        //$('#map-write-input-section').hide();
        $('#map-write-button-section').hide();

        loadMapMessagesAndShowMap();
    });
}

function bindInputEvents() {
    console.log(TAG, 'Called bindInputEvents');

    $('.input-container').focusin(function() {
        //console.log(TAG, 'focus in', $(this), $(this).find('.input-header').css('color'));
        $(this).find('.input-header').css( 'color', 'rgb(29, 161, 242)');
        $(this).find('.input-label').css( 'border-color', 'rgb(29, 161, 242)');
    });
    $('.input-container').focusout(function() {
        //console.log(TAG, 'focus out', $(this), $(this).find('.input-header').css('color'));
        if ($(this).find('.input-header').css('color') != 'rgb(224, 36, 94)') {
            $(this).find('.input-header').css('color', 'rgb(101, 119, 134)');
            $(this).find('.input-label').css( 'border-color', 'rgb(101, 119, 134)');
        }
    });
    
    $('.input-input').on('input',function(){
        let count = $(this).val().length;

        $(this).closest('.input-container').find('.input-count').text(count);
        if (count == 0 && ($(this).attr('id') != 'signup-description-input') && ($(this).attr('id') != 'edit-profile-description-input')) {
            //console.log(TAG, 'changing to red');
            $(this).closest('.input-container').find('.invalid-input-feedback').show();
            $(this).closest('.input-container').find('.input-header').css( 'color', 'rgb(224, 36, 94)');
            $(this).closest('.input-container').find('.input-label').css( 'border-color', 'rgb(224, 36, 94)');
        } else {
            //console.log(TAG, 'changing to blue');
            $(this).closest('.input-container').find('.invalid-input-feedback').hide();
            $(this).closest('.input-container').find('.input-header').css( 'color', 'rgb(29, 161, 242)');
            $(this).closest('.input-container').find('.input-label').css( 'border-color', 'rgb(29, 161, 242)');
        }

        if ($('#signup-password-input').val() !== '' && $('#signup-username-input').val() !== ''
            && $('#signup-name-input').val() !== '' && $('#signup-surname-input').val() !== ''){
            //console.log(TAG, 'signup submit button enabled');
            $('#signup-submit-button-container').find('button').prop('disabled',false);
        } else {
            //console.log(TAG, 'signup submit button disabled', $('#signup-password-input').val(), $('#signup-username-input').val(),
            //$('#signup-name-input').val(), $('#signup-surname-input').val());
            $('#signup-submit-button-container').find('button').prop('disabled',true);
        }

        if ($('#edit-profile-name-input').val() != '' && $('#edit-profile-surname-input').val() != '') {
            $('#edit-profile-submit-button-container').find('button').prop('disabled',false);
        } else {
            $('#edit-profile-submit-button-container').find('button').prop('disabled',true);
        }
    });
}

function bindSignupEvents() {
    console.log(TAG, 'Called bindSignupEvents');
    $('#signup-button').on('click', function() {
        $('#signup-error-div').hide();
        $('#signup-successful-div').hide();
    });

    $('#signup-submit-button').on('click', function(){
        console.log(TAG, 'Clicked on signup submit button');
        sendRegisterRequest();
    });
}

function bindProfileEvents() {
    console.log(TAG, 'Called bindProfileEvents');
    $('.profile-navbar-button-div').on('click', function() {
        $('.profile-navbar-button-div').css('color', 'rgb(101, 119, 134)');
        $('.profile-navbar-button-div').css('border-bottom-color', 'rgba(0, 0, 0, 0)');

        $(this).css('color', 'rgb(29, 161, 242)');
        $(this).css('border-bottom-color', 'rgb(29, 161, 242)');
    });

    $('#edit-profile-button').on('click', function() {
        console.log(TAG, 'Clicked edit-profile-button');
        $('#edit-profile-error-div').hide();
        $('#edit-profile-successful-div').hide();

        $('#edit-profile-modal').find('.input-header').each(function() {
            $(this).css('color', 'rgb(101, 119, 134)');
        });
        $('#edit-profile-modal').find('.input-label').each(function() {
            $(this).css( 'border-color', 'rgb(101, 119, 134)');
        });

        $('.invalid-input-feedback').hide();

        let img = stash.img ? 'data:image/jpeg;base64,' + stash.img : 'default_user_icon.png';
        console.log(TAG, 'img', img);

        $('#edit-profile-image').attr('src', img );

        $('#edit-profile-name-input').val(stash.name);
        $('#edit-profile-surname-input').val(stash.surname);
        $('#edit-profile-name-input').closest('.input-container').find('.input-count').text(stash.name.length);
        $('#edit-profile-surname-input').closest('.input-container').find('.input-count').text(stash.surname.length);
        
        $('#edit-profile-description-input').val( stash.description ? stash.description : '' );
        $('#edit-profile-description-input').closest('.input-container').find('.input-count').text( stash.description ? stash.description.length : 0);

        if ($('#edit-profile-name-input').val() != '' && $('#edit-profile-surname-input').val() != '') {
            $('#edit-profile-submit-button-container').find('button').prop('disabled',false);
        } else {
            $('#edit-profile-submit-button-container').find('button').prop('disabled',true);
        }
    });

    $('#profile-navbar-my-messages').on('click', function() {
        console.log(TAG, 'Clicked profile-navbar-my-messages');
        sendGetUserMessagesRequest();
    });

    $('#profile-navbar-followed-messages').on('click', function() {
        console.log(TAG, 'Clicked profile-navbar-followed-messages');
        sendGetFollowedMessagesRequest();
    });

    $('#profile-navbar-liked-messages').on('click', function() {
        console.log(TAG, 'Clicked profile-navbar-liked-messages');
        sendGetLikedMessagesRequest();
    });
}

function bindEditProfileEvents() {
    console.log(TAG, 'Called bindEditProfileEvents');

    let base64Image = stash.img;
    let removed = false;

    $('#edit-profile-image-input').change( function(e) {
        let file = $(this).prop('files')[0];
        getBase64(file);
    });

    $('#edit-profile-image-input-container').on('click', function() {
        console.log(TAG, 'Clicked edit-profile-image-input-container');
        $('#edit-profile-image-input').trigger('click');
    });

    $('#edit-profile-remove-image-input-container').on('click', function() {
        console.log(TAG, 'Clicked edit-profile-remove-image-input-container');
        removed = true;
        $('#edit-profile-image').attr('src', 'default_user_icon.png');
    });

    $('#edit-profile-submit-button').on('click', function() {
        console.log(TAG, 'Clicked edit-profile-submit-button');

        let img;
        if (removed) {
            img = null;
            removed = false;
        } else {
            img = base64Image ? base64Image.replace('data:image/jpeg;base64,', '') : stash.img;
        }

        sendProfileUpdateRequest($('#edit-profile-name-input').val(), $('#edit-profile-surname-input').val(), 
            $('#edit-profile-description-input').val(), img);
    });

    function getBase64(file) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            // reader.result.replace('data:image/jpeg;base64,', '')
            console.log(TAG, 'getBase64 result: ', reader.result);
            base64Image = reader.result;
            $('#edit-profile-image').attr('src', base64Image);
        };
        reader.onerror = function (error) {
            console.log(TAG, 'getBase64 error: ', error);
        };
     }
}

function bindSortFilterEvents() {
    console.log(TAG, 'Called bindSortFilterEvents');

    $('#map-coord-refresh-button').on('click', function() {
        console.log(TAG, 'Clicked map-coord-refresh-button');
        stash.adapter.refresh(getSortAndFilterObject());
    });

    $('#map-coord-messages-modal').on('hidden.bs.modal', function() {
        console.log(TAG, 'hidden map-coord-messages-modal');

        $('#map-coord-sort-key-input').val('time');
        $('#sort-order-radios-desc').prop('checked', true);
        $('#sort-order-radios-asc').prop('checked', false);
        $('#only-liked-input').prop('checked', false);

        $('#date-from-input').val('');
        $('#time-from-input').val('');
        $('#date-to-input').val('');
        $('#time-to-input').val('');

        $('#filter-icon-container').find('svg').remove();

        let newSvg =
            `
                <svg style='width: 25px; height: 25px' width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-filter-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                    <path fill-rule="evenodd" d="M7 11.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5z"/>
                </svg>
            `;

        $('#filter-icon-container').html($('#filter-icon-container').html() + newSvg);

        $('#filter-container').hide();

        setOnOpenFilter();
    });

    setOnOpenFilter();
}

function bindSearchEvents() {
    console.log(TAG, 'Called bindSearchEvents');

    $('#search-input').focusin(function() {
       console.log('search input focusin');
       $(this).closest('form').css('border-color', 'rgb(29, 161, 242)');
       $(this).closest('form').css('background-color', 'rgb(255, 255, 255)');
       $(this).closest('form').find('svg').css('color', 'rgb(29, 161, 242)');
    });
    $('#search-input').focusout(function() {
        console.log('search input focusout');
        $(this).closest('form').css('border-color', 'rgba(0, 0, 0, 0)');
        $(this).closest('form').css('background-color', 'rgb(230, 236, 240)');
        $(this).closest('form').find('svg').css('color', 'rgb(101, 119, 134)');
    });

    $('#search-form').on('submit', function(e) {
        e.preventDefault();
        console.log('search form submit');
        sendSearchRequest();
    });
}