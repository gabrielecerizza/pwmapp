var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST followed */
router.post('/', function(req, res, next) {
    console.log('Received followed request: ' + JSON.stringify(req.body));
    db.any({
        text: 'SELECT user_id, name, surname, img_version FROM registered_user WHERE user_id IN (\
            SELECT followed_user_id\
            FROM registered_user JOIN follow ON user_id = following_user_id\
            WHERE following_user_id = $1\
        );',
        values: [req.body.user_id]
        })
        .then(data => {
            res.json({ followed: data });
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;