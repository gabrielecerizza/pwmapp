var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST register user */
router.post('/', function(req, res, next) {
    console.log('Received register request: ' + JSON.stringify(req.body));

    db.one({
        text: 'INSERT INTO registered_user(username, password, name, surname, description, img_version) VALUES ($1, $2, $3, $4, $5, 0) RETURNING user_id',
        values: [req.body.username, req.body.password, req.body.name, req.body.surname, req.body.description]
        })
        .then(data => {
            console.log('USER_ID:', data.user_id);
            res.json(data);
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(500).send(error);
        });
});

module.exports = router;