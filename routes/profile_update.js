var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST profile_update */
router.post('/', function(req, res, next) {
    console.log('Received profile_update request: ' + JSON.stringify(req.body));
    db.one({
        text: 'SELECT * FROM profile_update($1, $2, $3, $4, $5)',
        values: [req.body.user_id, req.body.name, req.body.surname, req.body.img, req.body.description]
        })
        .then(data => {
            res.sendStatus(200);
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;