INSERT INTO registered_user (username, password, name, surname, img_version) 
    VALUES ('albertoalberti', 'pwdalberti', 'alberto', 'alberti', 0);
INSERT INTO registered_user (username, password, name, surname, img_version) 
    VALUES ('brunobruni', 'pwdbruni', 'bruno', 'bruni', 0);
INSERT INTO registered_user (username, password, name, surname, img_version) 
    VALUES ('carlocarli', 'pwdcarli', 'carlo', 'carli', 0);

SELECT user_id FROM registered_user WHERE username = 'albertoalberti' AND password = 'pwdalberti';

INSERT INTO message (user_id, latitude, longitude, message, time) 
    VALUES (1, 20.3, 90.4, 'Prova', NOW());
INSERT INTO message (user_id, latitude, longitude, message, time) 
    VALUES (1, 20.3, 90.4, 'Prova2', NOW());
INSERT INTO message (user_id, latitude, longitude, message, time) 
    VALUES (1, 20.3, 90.4, 'Prova3', NOW());
INSERT INTO message (user_id, latitude, longitude, message, time) 
    VALUES (2, 20.3, 91.4, 'b Prova', NOW());
INSERT INTO message (user_id, latitude, longitude, message, time) 
    VALUES (2, 20.3, 91.4, 'b Prova2', NOW());
INSERT INTO message (user_id, latitude, longitude, message, time) 
    VALUES (2, 20.3, 90.4, 'b Prova3', NOW());
INSERT INTO message (user_id, latitude, longitude, message, time) 
    VALUES (1, 45.47474, 9.17984, 'Milan 1', NOW());
INSERT INTO message (user_id, latitude, longitude, message, time) 
    VALUES (1, 45.47474, 9.17984, 'Milan 2', NOW());
INSERT INTO message (user_id, latitude, longitude, message, time) 
    VALUES (2, 45.47474, 9.17984, 'Milan 3', NOW());
INSERT INTO message (user_id, latitude, longitude, message, time) 
    VALUES (4, 45.47474, 9.17984, 'Milan 4', NOW());
INSERT INTO message (user_id, latitude, longitude, message, time) 
    VALUES (4, 45.47474, 9.17984, 'Milan 5', NOW());
INSERT INTO message (user_id, latitude, longitude, message, time) 
    VALUES (4, 45.47474, 9.17984, 'Milan 6', NOW());

SELECT * FROM img_update(12, 'prova');
SELECT * FROM img_update(11, 'prova');

SELECT encode(img, 'base64') AS img FROM registered_user;

SELECT * FROM follow(1, 2);

SELECT name, surname, r.user_id, latitude, longitude, message, time FROM registered_user AS r JOIN message AS m ON r.user_id = m.user_id WHERE r.user_id = 1;
SELECT name, surname, latitude, longitude, message, time FROM registered_user AS r JOIN message AS m ON r.user_id = m.user_id WHERE r.user_id = 1 OR r.user_id IN (
    SELECT f.followed_user_id
    FROM registered_user AS r JOIN follow AS f ON r.user_id = f.following_user_id
    WHERE following_user_id = 1
) ORDER BY time DESC;

SELECT name, surname, img_version FROM registered_user WHERE user_id IN (
    SELECT followed_user_id
    FROM registered_user JOIN follow ON user_id = following_user_id
    WHERE following_user_id = 1
);

SELECT encode(img, 'base64'), img_version FROM registered_user WHERE user_id = 1;

SELECT latitude, longitude, COUNT(*) FROM message GROUP BY latitude, longitude;
  
SELECT r.user_id, name, surname, encode(img, 'base64') AS img, message, time
FROM registered_user AS r JOIN message AS m ON r.user_id = m.user_id
WHERE (latitude - 20.3) < 0.00001 AND (longitude - 90.4) < 0.00001;

SELECT r.user_id, name, surname, message, latitude, longitude
FROM registered_user AS r JOIN message AS m ON r.user_id = m.user_id
WHERE (latitude - 20.3) < 0.00001 AND (longitude - 90.4) < 0.00001; 


WITH following(followed_id) AS (
    SELECT followed_user_id
    FROM follow
    WHERE following_user_id = 1
) SELECT r.user_id, followed_id, CASE WHEN EXISTS (
    SELECT *
    FROM follow
    WHERE following_user_id = 1
)
THEN TRUE
ELSE FALSE END
FROM registered_user AS r LEFT JOIN following AS f ON r.user_id = f.followed_id;
WHERE r.user_id = 1;

WITH following(followed_id) AS (
    SELECT followed_user_id
    FROM follow
    WHERE following_user_id = 1
) SELECT r.user_id, CASE WHEN followed_id IS NULL THEN false ELSE true END
FROM registered_user AS r LEFT JOIN following AS f ON r.user_id = f.followed_id;

WITH coord_mess AS (
    SELECT r.user_id, name, surname, message, latitude, longitude
    FROM registered_user AS r JOIN message AS m ON r.user_id = m.user_id
    WHERE ((latitude - 46.47474) < 0.00001) AND ((longitude - 9.17984) < 0.00001)
),
following(followed_id) AS (
    SELECT followed_user_id
    FROM follow
    WHERE following_user_id = 1
)
SELECT c.*, CASE WHEN followed_id IS NULL THEN false ELSE true END
FROM coord_mess AS c LEFT JOIN following AS f ON c.user_id = f.followed_id;

SELECT * FROM profile_update(13, 'Albert', 'Brecht', 'abbaa', 'My Description');
SELECT * FROM profile_update(13, 'Lukk', 'Arrtt', 'c2hvcnQ=', 'My Description2');
SELECT * FROM profile_update(13, 'Lukka', 'Arrtta', 'c2hvcnQ=', 'My Description3');
SELECT * FROM profile_update(1, 'Alberto', 'Alberti', null, 'Lorem Ipsum');

SELECT COUNT(*) FROM follow WHERE following_user_id = 1;

WITH following_table AS (
    SELECT following_user_id, COUNT(*) AS following_count
    FROM follow
    WHERE following_user_id = 13
    GROUP BY following_user_id
),
followed_table AS (
    SELECT followed_user_id, COUNT(*) AS followed_count
    FROM follow
    WHERE followed_user_id = 13
    GROUP BY followed_user_id
)
SELECT username, name, surname, img_version, description, coalesce(following_count, 0) AS following_count, coalesce(followed_count, 0) AS followed_count, encode(img::bytea, \'base64\') AS img
FROM following_table FULL JOIN followed_table ON following_user_id = followed_user_id
    RIGHT JOIN registered_user ON following_user_id = user_id
WHERE user_id = 13;

WITH mess_users AS (
    SELECT r.user_id
    FROM registered_user AS r JOIN message AS m ON r.user_id = m.user_id
    WHERE ABS(latitude - 45.652126) < 0.00001 AND ABS(longitude - 9.214375) < 0.00001
    GROUP BY r.user_id
)
SELECT r.user_id, img_version, encode(img::bytea, 'base64') AS img
FROM registered_user AS r JOIN mess_users AS m ON r.user_id = m.user_id;

INSERT INTO liked VALUES (1, 2);
INSERT INTO liked VALUES (13, 2), (13, 20), (1, 20), (2, 20), (4, 20);

WITH liked_num AS (
    SELECT message_id, COUNT(*)
    FROM liked
    GROUP BY message_id
)
SELECT r.user_id, name, surname, username, description, m.message_id, message, latitude, longitude, location, time, coalesce(l.count, 0) AS like_count
FROM registered_user AS r JOIN message AS m ON r.user_id = m.user_id LEFT JOIN liked_num AS l ON m.message_id = l.message_id
WHERE ABS(latitude - 45.652126) < 0.00001 AND ABS(longitude - 9.214375) < 0.00001;


WITH liked_num AS (
    SELECT message_id, COUNT(*)
    FROM liked
    GROUP BY message_id
),
coord_mess AS (
    SELECT r.user_id, name, surname, username, description, m.message_id, message, latitude, longitude, location, time, coalesce(l.count, 0) AS like_count
    FROM registered_user AS r JOIN message AS m ON r.user_id = m.user_id LEFT JOIN liked_num AS l ON m.message_id = l.message_id
    WHERE ABS(latitude - 45.652126) < 0.00001 AND ABS(longitude - 9.214375) < 0.00001
),
following(followed_id) AS (
    SELECT followed_user_id
    FROM follow
    WHERE following_user_id = 1
),
liking(message_id) AS (
    SELECT message_id
    FROM liked
    WHERE user_id = 1
)
SELECT c.*, CASE WHEN followed_id IS NULL THEN false ELSE true END AS followed, CASE WHEN l.message_id IS NULL THEN false ELSE true END AS liked 
FROM coord_mess AS c LEFT JOIN following AS f ON c.user_id = f.followed_id LEFT JOIN liking AS l ON c.message_id = l.message_id;

EXPLAIN ANALYZE
WITH following_table AS (
    SELECT following_user_id, COUNT(*) AS following_count
    FROM follow
    GROUP BY following_user_id
),
followed_table AS (
    SELECT followed_user_id, COUNT(*) AS followed_count
    FROM follow
    GROUP BY followed_user_id
)
SELECT r.user_id, coalesce(following_count, 0) AS following_count, coalesce(followed_count, 0) AS followed_count, img_version
FROM registered_user AS r LEFT JOIN following_table ON r.user_id = following_user_id LEFT JOIN followed_table ON r.user_id = followed_user_id
WHERE r.user_id IN (
    SELECT DISTINCT(m.user_id)
    FROM liked AS l JOIN message AS m ON l.message_id = m.message_id
    WHERE l.user_id = 13
);

EXPLAIN ANALYZE
WITH following_table AS (
    SELECT following_user_id, COUNT(*) AS following_count
    FROM follow
    GROUP BY following_user_id
),
followed_table AS (
    SELECT followed_user_id, COUNT(*) AS followed_count
    FROM follow
    GROUP BY followed_user_id
),
liked_messages_users AS (
    SELECT DISTINCT(m.user_id)
    FROM liked AS l JOIN message AS m ON l.message_id = m.message_id
    WHERE l.user_id = 13
)
SELECT r.user_id, coalesce(following_count, 0) AS following_count, coalesce(followed_count, 0) AS followed_count, img_version
FROM registered_user AS r JOIN liked_messages_users AS l ON r.user_id = l.user_id LEFT JOIN following_table ON r.user_id = following_user_id LEFT JOIN followed_table ON r.user_id = followed_user_id;


WITH liked_num AS (
    SELECT message_id, COUNT(*)
    FROM liked
    GROUP BY message_id
),
liked_messages AS (
    SELECT r.user_id, name, surname, username, description, m.message_id, message, latitude, longitude, location, time, coalesce(l.count, 0) AS like_count
    FROM registered_user AS r JOIN message AS m ON r.user_id = m.user_id JOIN liked ON m.message_id = liked.message_id LEFT JOIN liked_num AS l ON m.message_id = l.message_id
    WHERE liked.user_id = 13
),
following(followed_id) AS (
    SELECT followed_user_id
    FROM follow
    WHERE following_user_id = 13
)
SELECT l.*, CASE WHEN followed_id IS NULL THEN false ELSE true END AS followed, true AS liked
FROM liked_messages AS l LEFT JOIN following AS f ON l.user_id = f.followed_id
ORDER BY time DESC;


WITH mess_users AS (
    SELECT r.user_id
    FROM registered_user AS r JOIN message AS m ON r.user_id = m.user_id
    WHERE ABS(latitude - 45.652126) < 0.00001 AND ABS(longitude - 9.214375) < 0.00001
    GROUP BY r.user_id
),
following_table AS (
    SELECT following_user_id, COUNT(*) AS following_count
    FROM follow
    GROUP BY following_user_id
),
followed_table AS (
    SELECT followed_user_id, COUNT(*) AS followed_count
    FROM follow
    GROUP BY followed_user_id
),
followed_by_user(followed_by_user_id) AS (
    SELECT followed_user_id
    FROM follow
    WHERE following_user_id = 13
)
SELECT r.user_id, username, name, surname, description, CASE WHEN r.user_id IS IN followed_by_user.followed_by_user_id THEN true ELSE false END AS followed, coalesce(following_count, 0) AS following_count, coalesce(followed_count, 0) AS followed_count, img_version
FROM registered_user AS r JOIN mess_users AS m ON r.user_id = m.user_id LEFT JOIN followed_by_user AS f ON r.user_id = f.followed_by_user_id
    LEFT JOIN following_table ON r.user_id = following_user_id LEFT JOIN followed_table ON r.user_id = followed_user_id;



WITH liked_num AS (
    SELECT message_id, COUNT(*)
    FROM liked
    GROUP BY message_id
),
liking(message_id) AS (
    SELECT message_id
    FROM liked
    WHERE user_id = 15
)
SELECT m.user_id, m.message_id, message, latitude, longitude, location, time, coalesce(l.count, 0) AS like_count, CASE WHEN liking.message_id IS NULL THEN false ELSE true END AS liked
FROM message AS m LEFT JOIN liked_num AS l ON m.message_id = l.message_id LEFT JOIN liking ON m.message_id = liking.message_id
    WHERE ABS(latitude - 45.652126) < 0.00001 AND ABS(longitude - 9.214375) < 0.00001;


WITH following_table AS (
    SELECT following_user_id, COUNT(*) AS following_count
    FROM follow
    GROUP BY following_user_id
),
followed_table AS (
    SELECT followed_user_id, COUNT(*) AS followed_count
    FROM follow
    GROUP BY followed_user_id
),
followed_by_user(followed_by_user_id) AS (
    SELECT followed_user_id
    FROM follow
    WHERE following_user_id = $1
)
SELECT DISTINCT r.user_id, username, name, surname, description, CASE WHEN followed_by_user_id IS NULL THEN false ELSE true END AS followed, coalesce(following_count, 0) AS following_count, coalesce(followed_count, 0) AS followed_count, img_version, encode(img::bytea, \'base64\') AS img
FROM registered_user AS r LEFT JOIN following_table ON r.user_id = following_user_id LEFT JOIN followed_table ON r.user_id = followed_user_id
    LEFT JOIN followed_by_user ON r.user_id = followed_by_user_id
WHERE name ~* $2 OR surname ~* $2 OR username ~* $2;

