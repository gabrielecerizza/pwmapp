var express = require('express');
var router = express.Router();
var db = require('../public/javascripts/db');

/* POST get_user_data */
router.post('/', function(req, res, next) {
    console.log('Received get_user_data request: ' + JSON.stringify(req.body));
    db.one({
        text: 'SELECT username, name, surname, encode(img::bytea, \'base64\') AS img, img_version FROM registered_user WHERE user_id = $1',
        values: [req.body.queried_user_id]
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.log('ERROR:', error);
            res.status(400).send(error);
        });
});

module.exports = router;