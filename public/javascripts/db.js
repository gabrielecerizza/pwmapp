const pgp = require('pg-promise')(/*options*/);
const cn = {
    host: 'localhost', // 'localhost' is the default;
    port: 5432, // 5432 is the default;
    database: 'pwmdb',
    user: 'gabriele',
    password: 'pwmpwd'
};
const db = pgp(cn);

module.exports = db;