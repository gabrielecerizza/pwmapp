CREATE OR REPLACE FUNCTION img_update(q_user_id INTEGER, q_img TEXT) RETURNS CHAR AS $$
    DECLARE 
        old_img_version registered_user.img_version%TYPE;
    BEGIN
        PERFORM * FROM registered_user WHERE user_id = q_user_id;
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Nonexistent user_id: %', q_user_id; 
            RETURN '1';
        END IF;

        SELECT img_version INTO old_img_version FROM registered_user WHERE user_id = q_user_id;
        UPDATE registered_user SET img = decode(q_img, 'base64') WHERE user_id = q_user_id;
        UPDATE registered_user SET img_version = (old_img_version + 1) WHERE user_id = q_user_id;
        RETURN '0';
    END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION profile_update(q_user_id INTEGER, q_name VARCHAR, q_surname VARCHAR, q_img TEXT, q_description TEXT) RETURNS CHAR AS $$
    DECLARE 
        byte64_img TEXT;
    BEGIN
        PERFORM * FROM registered_user WHERE user_id = q_user_id;
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Nonexistent user_id: %', q_user_id; 
            RETURN '1';
        END IF;

        UPDATE registered_user SET name = q_name, surname = q_surname, description = q_description WHERE user_id = q_user_id;

        IF q_img IS NULL THEN
            RAISE NOTICE 'Removing image';
            UPDATE registered_user SET img = NULL WHERE user_id = q_user_id;
            UPDATE registered_user SET img_version = (img_version + 1) WHERE user_id = q_user_id;
        ELSE 
            SELECT encode(img::bytea, 'base64') INTO byte64_img FROM registered_user WHERE user_id = q_user_id;
            IF byte64_img IS NULL OR byte64_img <> q_img THEN
                UPDATE registered_user SET img = decode(q_img, 'base64') WHERE user_id = q_user_id;
                UPDATE registered_user SET img_version = (img_version + 1) WHERE user_id = q_user_id;
            ELSE
                RAISE NOTICE 'Should be equal: % %', byte64_img, q_img;
            END IF;
        END IF; 
        RETURN '0';

        EXCEPTION
            WHEN SQLSTATE '22023' THEN
				RAISE EXCEPTION 'Invalid byte64 sequence';
				RETURN '2';
    END;

$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION follow(my_user_id INTEGER, other_user_id INTEGER) RETURNS CHAR AS $$
    BEGIN
        IF my_user_id = other_user_id THEN
            RAISE EXCEPTION USING DETAIL = 'You cannot follow yourself'; 
            RETURN '1';
        END IF;

        PERFORM * FROM follow WHERE following_user_id = my_user_id AND followed_user_id = other_user_id;
        IF FOUND THEN
            RAISE EXCEPTION USING DETAIL = 'You are already following this user'; 
            RETURN '2';
        END IF;

        PERFORM * FROM registered_user WHERE user_id = other_user_id;
        IF NOT FOUND THEN
            RAISE EXCEPTION USING DETAIL = 'User not found'; 
            RETURN '3';
        END IF;

        INSERT INTO follow VALUES (my_user_id, other_user_id);
        RETURN '0';
    END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION unfollow(my_user_id INTEGER, other_user_id INTEGER) RETURNS CHAR AS $$
    BEGIN
        PERFORM * FROM registered_user WHERE user_id = other_user_id;
        IF NOT FOUND THEN
            RAISE EXCEPTION USING DETAIL = 'User not found'; 
            RETURN '1';
        END IF;

        PERFORM * FROM follow WHERE following_user_id = my_user_id AND followed_user_id = other_user_id;
        IF NOT FOUND THEN
            RAISE EXCEPTION USING DETAIL = 'You are not following this user'; 
            RETURN '2';
        END IF;

        DELETE FROM follow WHERE following_user_id = my_user_id AND followed_user_id = other_user_id;
        RETURN '0';
    END;
$$ LANGUAGE plpgsql;